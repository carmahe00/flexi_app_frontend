import axios from 'axios'
import types from '../types/pedidosType'

const baseUrl = process.env.REACT_APP_API_URL

export const listPedidosP = () => {
    return async (dispatch, getState) => {
        try {

            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.get(`${baseUrl}/pedidos-pendiente`, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            dispatch({
                type: types.pedidosPSuccess,
                payload: data
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.pedidosPFail,
                payload: 'Algo salio mal con los pedidos'

            })
        }
    }
}


export const getPedidosDetalle = () => {
    return async (dispatch, getState) => {
        try {

            const { userLogin: { userInfo }, pedidoPDetails } = getState()


            const { data } = await axios.get(`${baseUrl}/pedidos-pendiente/detalle`, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })
            if (data.pedidoP.length !== pedidoPDetails.pedidosDetalleDetail.length || pedidoPDetails.pedidosDetalleDetail.length === 0)
                dispatch({
                    type: types.pedidosDetallePSuccess,
                    payload: data
                })
        } catch (error) {
            dispatch({
                type: types.pedidosDetallePFail,
                payload: 'Algo salio mal con los pedidos'

            })
        }
    }
}

export const getDetailPedido = async (token, idPedido, idDespacho, setLoading) => {
    try {
        setLoading(true)
        const data = await axios.get(`${baseUrl}/pedidos-pendiente/detalle/${idPedido}/${idDespacho}`, {
            headers: {
                'Authorization': `${token}`
            }
        })
        setLoading(false)
        return data
    } catch (error) {
        setLoading(false)
        return error.response.data
    }
}

export const changeDetailPedido = (idPedido, idDespacho) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.changeStatePeiddoDetalleRequest
            })
            const { userLogin: { userInfo } } = getState()

            const { data } = await axios.put(`${baseUrl}/pedidos-pendiente/${idPedido}/${idDespacho}`, {}, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            dispatch({
                type: types.changeStatePeiddoDetalleSuccess,
                payload: data
            })
            dispatch(listPedidosP())
        } catch (error) {
            dispatch({
                type: types.changeStatePeiddoDetalleFail,
                payload: 'Algo salió mal'
            })
        }
    }
}

export const closeModalPedido = () => ({ type: types.pedidoPReset });
import axios from 'axios'
import types from '../types/puestosType'

const baseUrl = process.env.REACT_APP_API_URL

export const listPuestos = () => {
    return async (dispatch) => {
        try {
            dispatch({
                type: types.puestoListRequest
            })

            const { data } = await axios.get(`${baseUrl}/puestos`)

            dispatch({
                type: types.puestoListSuccess,
                payload: data
            })
        } catch (error) {
            dispatch({
                type: types.puestoListFail,
                payload: 'Algo salio Mal con los puestos'

            })
        }
    }
}
import axios from 'axios'
import types from '../types/productoTypes'

const baseUrl = process.env.REACT_APP_API_URL

export const listProductos = (idCategoria) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.productoListRequest
            })

            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.get(`${baseUrl}/cobros/${idCategoria}`, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            dispatch({
                type: types.productoListSuccess,
                payload: data
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.productoListFail,
                payload: 'Algo salio mal con los productos'

            })
        }
    }
}

export const createProducto = (idCategoria) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.productoCreateRequest
            })
            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.post(`${baseUrl}/cobros/${idCategoria}`, {}, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            dispatch({
                type: types.productoCreatSuccess,
                payload: data
            })
        } catch (error) {
            dispatch({
                type: types.productoCreateFail,
                payload: 'Algo salio Mal con los productos'

            })
        }
    }
}

export const updateproducto = (props, id_grupo) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.productoUpdateSuccess
            })
            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.put(`${baseUrl}/cobros/${id_grupo}`, props, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            
            dispatch({
                type: types.productoUpdateSuccess,
                payload: data
            })
            
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.productoUpdateFail,
                payload: 'Algo salio Mal con los productos'

            })
        }
    }
}

export const listPublicProductos = (idCategoria) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: types.productoPublicListRequest
            })

            const { data } = await axios.get(`${baseUrl}/cobros/public/${idCategoria}`)

            dispatch({
                type: types.productoPublicListSuccess,
                payload: data
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.productoPublicListFail,
                payload: 'Algo salio mal con los productos'

            })
        }
    }
}
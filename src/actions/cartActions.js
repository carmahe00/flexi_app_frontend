import types from '../types/cartTypes'

export const addToCart = (producto, sugerencia) => {
    return async (dispatch, getState) => {

        dispatch({
            type: types.cartAddItem,
            payload: { producto, sugerencia: sugerencia === '' ? 'Normal' : sugerencia }
        })

        localStorage.setItem('cartItems', JSON.stringify(getState().cart.cartItems))
        localStorage.setItem('sugerencias', JSON.stringify(getState().cart.sugerencias))
    }
}

export const removeFromCart = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: types.cartRemoveItem,
            payload: id
        })

        localStorage.setItem('cartItems', JSON.stringify(getState().cart.cartItems))
        localStorage.setItem('sugerencias', JSON.stringify(getState().cart.sugerencias))
    }
}

export const saveShippingAddress = (data) => {
    return async (dispatch) => {
        dispatch({
            type: types.cartSaveShippingAddress,
            payload: data
        })

        localStorage.setItem('shippingAddress', JSON.stringify(data))
    }
}
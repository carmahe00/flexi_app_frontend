import axios from 'axios'
import types from '../types/pedidoType'
import cartTypes from '../types/cartTypes'

const baseUrl = process.env.REACT_APP_API_URL

export const createPedido = (pedido) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: types.pedidoCreateRequest
            })

            await axios.post(`${baseUrl}/pedidos`, pedido)

            dispatch({
                type: types.pedidoCreateSuccess,
                payload: 'Se acaba de crear su pedido'
            })

            dispatch({
                type: cartTypes.cartReset
            })
            localStorage.removeItem('cartItems')
            localStorage.removeItem('sugerencias')
        } catch (error) {
            dispatch({
                type: types.pedidoCreateFail,
                payload: 'Algo salió mal'
            })
        }
    }
}
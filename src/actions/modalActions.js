import types from "../types/modalTypes";

export const openModalCategoria = (categoria) => ({ type: types.modalOpenCategoria, payload: categoria });

export const closeModalCategoria = () => ({ type: types.modalCloseCategoria });

export const openModalConfiguracion = (configuracion) => ({ type: types.modalOpenConfiguracion, payload: configuracion });

export const closeModalConfiguracion = () => ({ type: types.modalCloseConfiguracion });

export const openModalProducto = (producto) => ({ type: types.modalOpenProducto, payload: producto });

export const closeModalProducto = () => ({ type: types.modalCloseProducto });

export const openModalProductoPublic = (producto) => ({ type: types.modalOpenProductoPublic, payload: producto });

export const closeModalProductoPublic = () => ({ type: types.modalCloseProductoPublic });
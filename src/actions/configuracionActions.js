import axios from 'axios'
import types from '../types/configuracionType'

const baseUrl = process.env.REACT_APP_API_URL

export const listConfiguracion = () => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.configuracionListRequest
            })

            
            const { data } = await axios.get(`${baseUrl}/configuracion`)
            
            dispatch({
                type: types.configuracionListSuccess,
                payload: data
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.configuracionListFail,
                payload: 'Algo salio mal con las configuracion'

            })
        }
    }
}


export const updateConfiguracion = (props, nit) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.configuracionUpdateSuccess
            })
            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.put(`${baseUrl}/configuracion/${nit}`, props, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })
            
            dispatch({
                type: types.configuracionUpdateSuccess,
                payload: data
            })
            dispatch(listConfiguracion())
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.configuracionUpdateFail,
                payload: 'Algo salio Mal con las configuracion'

            })
        }
    }
}

import axios from 'axios'
import types from '../types/categoriaTypes'

const baseUrl = process.env.REACT_APP_API_URL

export const listCategoria = (size, page) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.categoriaListRequest
            })

            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.get(`${baseUrl}/categorias?page=${page}&size=${size}`, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })
            dispatch({
                type: types.categoriaListSuccess,
                payload: data
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.categoriaListFail,
                payload: 'Algo salio mal con las categorias'

            })
        }
    }
}

export const createCategoria = () => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.categoriaCreateRequest
            })
            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.post(`${baseUrl}/categorias`, {}, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })

            dispatch({
                type: types.categoriaCreatSuccess,
                payload: data
            })
        } catch (error) {
            dispatch({
                type: types.categoriaCreateFail,
                payload: 'Algo salio Mal con las categorias'

            })
        }
    }
}

export const updateCategoria = (props, id_grupo) => {
    return async (dispatch, getState) => {
        try {
            dispatch({
                type: types.categoriaUpdateSuccess
            })
            const { userLogin: { userInfo } } = getState()
            const { data } = await axios.put(`${baseUrl}/categorias/${id_grupo}`, props, {
                headers: {
                    'Authorization': `${userInfo.token}`
                }
            })
            
            dispatch({
                type: types.categoriaUpdateSuccess,
                payload: data
            })
            dispatch(listCategoria(5, 0))
        } catch (error) {
            console.log(error)
            dispatch({
                type: types.categoriaUpdateFail,
                payload: 'Algo salio Mal con las categorias'

            })
        }
    }
}

export const listPublicCategoria = () => {
    return async (dispatch) => {
        try {
            dispatch({
                type: types.categoriaPublicListRequest
            })
            
            const { data } = await axios.get(`${baseUrl}/categorias/public`)

            dispatch({
                type: types.categoriaPublicListSuccess,
                payload: data
            })
        } catch (error) {
            dispatch({
                type: types.categoriaPublicListFail,
                payload: 'Algo salio Mal con las categorias'

            })
        }
    }
}
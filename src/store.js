import thunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension'
import { applyMiddleware, combineReducers, createStore } from "redux";
import { userLoginReducer, userRecoverPasswordReducer, userRegisterReducer } from "./reducer/userReducer";
import { changePedidoStateReducer, pedidoPReducer, pedidosReducer } from './reducer/pedidoPReducer'
import { categoriaCreateReducer, categoriaDetailReducer, categoriaListReducer, categoriaPublicListReducer, categoriaUpdateReducer } from "./reducer/categoriaReeducer";
import { modalReducer, modalProductoReducer, modalProductoPublicReducer, modalConfiguracionReducer } from './reducer/modalReducer'
import { productoCreateReducer, productoDetailReducer, productoListReducer, productoUpdateReducer } from "./reducer/productoReducer";
import { productoPublicListReducer } from "./reducer/productoPublicReducer";
import { cartReducer } from "./reducer/cartReducer";
import { puestoListReducer } from "./reducer/puestoReducer";
import { createPedidoReducer } from "./reducer/pedidoReducer";
import { configuracionCreateReducer, configuracionDetailReducer, configuracionListReducer, configuracionUpdateReducer } from "./reducer/configuracionReducer";

const cartItemsFromStorage = localStorage.getItem('cartItems')
    ? JSON.parse(localStorage.getItem('cartItems'))
    : []

const sugerenciasFromStorage = localStorage.getItem('sugerencias')
    ? JSON.parse(localStorage.getItem('sugerencias'))
    : []

const shippingAddressFromStorage = localStorage.getItem('shippingAddress')
    ? JSON.parse(localStorage.getItem('shippingAddress'))
    : {}

const userInfoFromStorage = localStorage.getItem('userInfo')
    ? JSON.parse(localStorage.getItem('userInfo'))
    : null

const initialState = {
    cart: {
        cartItems: cartItemsFromStorage,
        sugerencias: sugerenciasFromStorage,
        shippingAddress: shippingAddressFromStorage
    },
    userLogin: { userInfo: userInfoFromStorage },
}

const reducer = combineReducers({

    cart: cartReducer,

    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userRecovery: userRecoverPasswordReducer,

    pedidoPDetails: pedidoPReducer,
    pedidoP: pedidosReducer,
    changePedidoState: changePedidoStateReducer,

    categoriaList: categoriaListReducer,
    categoriaDetail: categoriaDetailReducer,
    categoriaCreate: categoriaCreateReducer,
    categoriaUpdate: categoriaUpdateReducer,

    configuracionList: configuracionListReducer,
    configuracionDetail: configuracionDetailReducer,
    configuracionCreate: configuracionCreateReducer,
    configuracionUpdate: configuracionUpdateReducer,

    categoriaPublicList: categoriaPublicListReducer,

    modalForm: modalReducer,
    modalConfiguracion: modalConfiguracionReducer,
    modalProductoForm: modalProductoReducer,
    modalProductoPublicForm: modalProductoPublicReducer,

    productoList: productoListReducer,
    productoDetail: productoDetailReducer,
    productoCreate: productoCreateReducer,
    productoUpdate: productoUpdateReducer,

    productoPublicList: productoPublicListReducer,

    puestoList: puestoListReducer,

    pedidoCreate: createPedidoReducer
})

const middleware = [thunk]

const store = createStore(reducer, initialState,
    composeWithDevTools(applyMiddleware(...middleware)))
export default store

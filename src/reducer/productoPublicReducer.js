import types from "../types/productoTypes";

export const productoPublicListReducer = (state = { productos: [] }, action) => {
    switch (action.type) {
        case types.productoPublicListRequest:
            return { loading: true, productos: [] }
        case types.productoPublicListSuccess:
            return { loading: false, productos: action.payload }
        case types.productoPublicListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}
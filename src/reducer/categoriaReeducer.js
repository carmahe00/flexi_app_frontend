import types from "../types/categoriaTypes";

export const categoriaListReducer = (state = { categorias: [] }, action) => {
    switch (action.type) {
        case types.categoriaListRequest:
            return { loading: true, categorias: [] }
        case types.categoriaListSuccess:
            return { loading: false, categorias: action.payload.pedidos }
        case types.categoriaListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const categoriaDetailReducer = (state = { categoria: {} }, action) => {
    switch (action.type) {
        case types.categoriaDetailsSRequest:
            return { loading: true, ...state }
        case types.categoriaDetailSuccess:
            return { loading: false, categorias: action.payload }
        case types.categoriaDetailsFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const categoriaCreateReducer = (state = { categoria: {} }, action) => {
    switch (action.type) {
        case types.categoriaCreateRequest:
            return { loading: true, ...state }
        case types.categoriaCreatSuccess:
            return { loading: false, categoria: action.payload, success: true }
        case types.categoriaCreateFail:
            return { loading: false, error: action.payload }
        case types.categoriaCreateReset:
            return { categoria: {} }
        default:
            return state
    }
}


export const categoriaUpdateReducer = (state = { categoria: {} }, action) => {
    switch (action.type) {
        case types.categoriaUpdateRequest:
            return { loading: true }
        case types.categoriaUpdateSuccess:
            return { loading: false, success: true, categoria: action.payload }
        case types.categoriaUpdateFail:
            return { loading: false, error: action.payload }
        case types.categoriaUpdateReset:
            return { categoria: {} }
        default:
            return state
    }
}

export const categoriaPublicListReducer = (state = { categorias: [] }, action) => {
    switch (action.type) {
        case types.categoriaPublicListRequest:
            return { loading: true, categorias: [] }
        case types.categoriaPublicListSuccess:
            return { loading: false, categorias: action.payload }
        case types.categoriaPublicListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}
import types from "../types/modalTypes";

export const modalReducer = (state = { categoriaModalOpen: false, categoriaModal: {} }, action) => {

    switch (action.type) {
        case types.modalOpenCategoria:
            return {
                ...state,
                categoriaModalOpen: true,
                categoriaModal: action.payload
            }
        case types.modalCloseCategoria:
            return {
                ...state,
                categoriaModalOpen: false,
                categoriaModal: {}
            }
        default:
            return state;
    }
}

export const modalConfiguracionReducer = (state = { configuracionModalOpen: false, configuracionModal: {} }, action) => {

    switch (action.type) {
        case types.modalOpenConfiguracion:
            return {
                ...state,
                configuracionModalOpen: true,
                configuracionModal: action.payload
            }
        case types.modalCloseConfiguracion:
            return {
                ...state,
                configuracionModalOpen: false,
                configuracionModal: {}
            }
        default:
            return state;
    }
}

export const modalProductoReducer = (state = { productoModalOpen: false, productoModal: {} }, action) => {

    switch (action.type) {
        case types.modalOpenProducto:
            return {
                ...state,
                productoModalOpen: true,
                productoModal: action.payload
            }
        case types.modalCloseProducto:
            return {
                
                productoModalOpen: false,
                productoModal: {}
            }
        default:
            return state;
    }
}

export const modalProductoPublicReducer = (state = { productoPublicModalOpen: false, productoPublicModal: {} }, action) => {

    switch (action.type) {
        case types.modalOpenProductoPublic:
            return {
                ...state,
                productoPublicModalOpen: true,
                productoPublicModal: action.payload
            }
        case types.modalCloseProductoPublic:
            return {
                ...state,
                productoPublicModalOpen: false,
                productoPublicModal: {}
            }
        default:
            return state;
    }
}
import types from '../types/puestosType'

export const puestoListReducer = (state = { puestos: [] }, action) => {
    switch (action.type) {
        case types.puestoListRequest:
            return { loading: true, puestos: [] }
        case types.puestoListSuccess:
            return { loading: false, puestos: action.payload }
        case types.puestoListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}
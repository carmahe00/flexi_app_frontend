import types from '../types/cartTypes';

export const cartReducer = (
    state = { cartItems: [], sugerencias: [], shippingAddress:{} },
    action
) => {
    switch (action.type) {
        case types.cartAddItem:
            let item = action.payload.producto
            let sugerencia = action.payload.sugerencia
            let existItem = state.cartItems.find(x => x.id_cobro === item.id_cobro)

            if (existItem)
                return {
                    ...state,
                    cartItems: state.cartItems.map(x =>
                        (x.id_cobro === existItem.id_cobro) ? { ...item, count: x.count + item.count, sugerencias: `${x.sugerencias}, ${item.sugerencias}` } : x
                    ),
                    sugerencias: [...state.sugerencias, { id_cobro: item.id_cobro, sugerencia, nombre: item.nombre }]
                }
            else
                return {
                    ...state,
                    cartItems: [...state.cartItems, { ...item }],
                    sugerencias: [...state.sugerencias, { id_cobro: item.id_cobro, sugerencia, nombre: item.nombre }]
                }
        case types.cartRemoveItem:
            return {
                ...state,
                cartItems: state.cartItems.filter(x => x.id_cobro !== action.payload),
                sugerencias: state.sugerencias.filter(x => x.id_cobro !== action.payload)
            }
        case types.cartSaveShippingAddress:
            return {
                ...state,
                shippingAddress: action.payload
            }
        case types.cartReset:
            return {
                cartItems: [], sugerencias: [], shippingAddress:{}
            }
        default:
            return state;
    }
}
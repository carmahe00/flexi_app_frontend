import types from '../types/pedidosType'

export const pedidosReducer = (state = { pedidoPInfo: [] }, action) => {
    switch (action.type) {
        case types.pedidosPRequest:
            return { ...state, loading: true, pedidoPInfo: [] }
        case types.pedidosPSuccess:
            return { ...state, loading: false, pedidoPInfo: action.payload }
        case types.pedidosPFail:
            return { ...state, loading: false, error: action.payload }
        case types.pedidosPReset:
            return {}
        default:
            return state
    }
}

export const pedidoPReducer = (state = { pedidosDetalleDetail: [] }, action) => {
    switch (action.type) {
        case types.pedidosDetallePRequest:
            return { ...state, loadingPedidoDetail: true, pedidosDetalleDetail: [] }
        case types.pedidosDetallePSuccess:
            return { ...state, loadingPedidoDetail: false, pedidosDetalleDetail: action.payload.pedidoP}
        case types.pedidosDetallePFail:
            return { ...state, loadingPedidoDetail: false, error: action.payload }
        default:
            return state
    }
}


export const changePedidoStateReducer = (state = {loading: false}, action) => {
    switch (action.type) {
        case types.changeStatePeiddoDetalleRequest:
            return { loading: true }
        case types.changeStatePeiddoDetalleSuccess:
            return { loading: false, pedidoResponse: action.payload }
        case types.changeStatePeiddoDetalleFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}
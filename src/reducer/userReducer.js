import types from '../types/userTypes'

export const userLoginReducer = (state = { loading: false, userInfo: {} }, action) => {
    switch (action.type) {
        case types.userLoginRequest:
            return { loading: true }
        case types.userLoginSuccess:
            return { loading: false, userInfo: action.payload }
        case types.userLoginFail:
            return { loading: false, error: action.payload }
        case types.userRenewToken:
            return { ...state, userInfo: action.payload, loading: false }
        case types.userLogout:
            return {}
        default:
            return state
    }
}


export const userRegisterReducer = (state = {}, action) => {
    switch (action.type) {
        case types.userRegisterRequest:
            return { loading: true }
        case types.userRegisterSuccess:
            return { loading: false, userInfo: action.payload }
        case types.userRegisterFail:
            return { loading: false, error: action.payload }
        case types.userRegisterReset:
            return {}
        default:
            return state
    }
}

export const userRecoverPasswordReducer = (state = {}, action) => {
    switch (action.type) {
        case types.userRecoveryPasswordRequest:
            return { loading: true }
        case types.userRecoveryPasswordSuccess:
            return { loading: false, userInfo: action.payload }
        case types.userRecoveryPasswordFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

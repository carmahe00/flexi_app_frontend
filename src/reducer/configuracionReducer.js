import types from "../types/configuracionType";

export const configuracionListReducer = (state = { configuraciones: [] }, action) => {
    switch (action.type) {
        case types.configuracionListRequest:
            return { loading: true, configuraciones: [] }
        case types.configuracionListSuccess:
            return { loading: false, configuraciones: action.payload }
        case types.configuracionListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const configuracionDetailReducer = (state = { configuracion: {} }, action) => {
    switch (action.type) {
        case types.configuracionDetailsSRequest:
            return { loading: true, ...state }
        case types.configuracionDetailSuccess:
            return { loading: false, configuracion: action.payload }
        case types.configuracionDetailsFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const configuracionCreateReducer = (state = { configuracion: {} }, action) => {
    switch (action.type) {
        case types.configuracionCreateRequest:
            return { loading: true, ...state }
        case types.configuracionCreatSuccess:
            return { loading: false, configuracion: action.payload, success: true }
        case types.configuracionCreateFail:
            return { loading: false, error: action.payload }
        case types.configuracionCreateReset:
            return { configuracion: {} }
        default:
            return state
    }
}


export const configuracionUpdateReducer = (state = { configuracion: {} }, action) => {
    switch (action.type) {
        case types.configuracionUpdateRequest:
            return { loading: true }
        case types.configuracionUpdateSuccess:
            return { loading: false, success: true, configuracion: action.payload }
        case types.configuracionUpdateFail:
            return { loading: false, error: action.payload }
        case types.configuracionUpdateReset:
            return { configuracion: {} }
        default:
            return state
    }
}

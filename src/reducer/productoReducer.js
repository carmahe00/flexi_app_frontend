import types from "../types/productoTypes";

export const productoListReducer = (state = { productos: [] }, action) => {
    switch (action.type) {
        case types.productoListRequest:
            return { loading: true, productos: [] }
        case types.productoListSuccess:
            return { loading: false, productos: action.payload }
        case types.productoListFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const productoDetailReducer = (state = { producto: {} }, action) => {
    switch (action.type) {
        case types.productoDetailsSRequest:
            return { loading: true, ...state }
        case types.productoDetailSuccess:
            return { loading: false, productos: action.payload }
        case types.productoDetailsFail:
            return { loading: false, error: action.payload }
        default:
            return state
    }
}

export const productoCreateReducer = (state = { producto: {} }, action) => {
    switch (action.type) {
        case types.productoCreateRequest:
            return { loading: true, ...state }
        case types.productoCreatSuccess:
            return { loading: false, producto: action.payload, success: true }
        case types.productoCreateFail:
            return { loading: false, error: action.payload }
        case types.productoCreateReset:
            return { producto: {} }
        default:
            return state
    }
}


export const productoUpdateReducer = (state = { producto: {} }, action) => {
    switch (action.type) {
        case types.productoUpdateRequest:
            return { loading: true }
        case types.productoUpdateSuccess:
            return { loading: false, success: true, producto: action.payload }
        case types.productoUpdateFail:
            return { loading: false, error: action.payload }
        case types.productoUpdateReset:
            return { producto: {} }
        default:
            return state
    }
}
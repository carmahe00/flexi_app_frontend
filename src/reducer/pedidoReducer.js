import types from '../types/pedidoType'

export const createPedidoReducer = (state = { loading: false }, action) => {
    switch (action.type) {
        case types.pedidoCreateRequest:
            return { loading: true }
        case types.pedidoCreateSuccess:
            return { loading: false, mensaje: action.payload }
        case types.pedidoCreateFail:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}

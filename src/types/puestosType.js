const types = {
    puestoListRequest: '[puesto] PUESTO_LIST_REQUEST',
    puestoListSuccess: '[puesto] PUESTO_LIST_SUCCESS',
    puestoListFail: '[puesto] PUESTO_LIST_FAIL',
}
export default types
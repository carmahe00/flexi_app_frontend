const types = {
    modalOpenCategoria: '[ui] Open modalCategoria',
    modalCloseCategoria: '[ui] Close modalCategoria',

    modalOpenConfiguracion: '[ui] Open modalConfiguracion',
    modalCloseConfiguracion: '[ui] Close modalConfiguracion',

    modalOpenProducto: '[ui] Open modalProducto',
    modalCloseProducto: '[ui] Close modalProducto',

    modalOpenProductoPublic: '[ui] Open modalProductoPublic',
    modalCloseProductoPublic: '[ui] Close modalProductoPublic'
}

export default types
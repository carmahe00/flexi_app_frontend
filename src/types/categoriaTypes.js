const types = {
    categoriaListRequest: '[categoria] CATEGORIA_LIST_REQUEST',
    categoriaListSuccess: '[categoria] CATEGORIA_LIST_SUCCESS',
    categoriaListFail: '[categoria] CATEGORIA_LIST_FAIL',

    categoriaDetailsSRequest: '[categoria] CATEGORIA_DETAILS_REQUEST',
    categoriaDetailSuccess: '[categoria] CATEGORIA_DETAILS_SUCCESS',
    categoriaDetailsFail: '[categoria] CATEGORIA_DETAILS_FAIL',

    categoriaDeletesSRequest: '[categoria] CATEGORIA_DELETE_REQUEST',
    categoriaDeleteSuccess: '[categoria] CATEGORIA_DELETE_SUCCESS',
    categoriaDeletesFail: '[categoria] CATEGORIA_DELETE_FAIL',


    categoriaCreateRequest: '[categoria] CATEGORIA_CREATE_REQUEST',
    categoriaCreatSuccess: '[categoria] CATEGORIA_CREATE_SUCCESS',
    categoriaCreateFail: '[categoria] CATEGORIA_CREATE_FAIL',
    categoriaCreateReset: '[categoria] CATEGORIA_CREATE_RESET',

    categoriaUpdateRequest: '[categoria] CATEGORIA_UPDATE_REQUEST',
    categoriaUpdateSuccess: '[categoria] CATEGORIA_UPDATE_SUCCESS',
    categoriaUpdateFail: '[categoria] CATEGORIA_UPDATE_FAIL',
    categoriaUpdateReset: '[categoria] CATEGORIA_UPDATE_RESET',

    categoriaPublicListRequest: '[categoriaPublic] CATEGORIA_PUBLIC_LIST_REQUEST',
    categoriaPublicListSuccess: '[categoriaPublic] CATEGORIA_PUBLIC_LIST_SUCCESS',
    categoriaPublicListFail: '[categoriaPublic] CATEGORIA_PUBLIC_LIST_FAIL',
}

export default types
const types = {
    configuracionListRequest: '[configuracion] CONFIGURACION_LIST_REQUEST',
    configuracionListSuccess: '[configuracion] CONFIGURACION_LIST_SUCCESS',
    configuracionListFail: '[configuracion] CONFIGURACION_LIST_FAIL',

    configuracionDetailsSRequest: '[configuracion] CONFIGURACION_DETAILS_REQUEST',
    configuracionDetailSuccess: '[configuracion] CONFIGURACION_DETAILS_SUCCESS',
    configuracionDetailsFail: '[configuracion] CONFIGURACION_DETAILS_FAIL',

    configuracionCreateRequest: '[configuracion] CONFIGURACION_CREATE_REQUEST',
    configuracionCreatSuccess: '[configuracion] CONFIGURACION_CREATE_SUCCESS',
    configuracionCreateFail: '[configuracion] CONFIGURACION_CREATE_FAIL',
    configuracionCreateReset: '[configuracion] CONFIGURACION_CREATE_RESET',

    configuracionUpdateRequest: '[configuracion] CONFIGURACION_UPDATE_REQUEST',
    configuracionUpdateSuccess: '[configuracion] CONFIGURACION_UPDATE_SUCCESS',
    configuracionUpdateFail: '[configuracion] CONFIGURACION_UPDATE_FAIL',
    configuracionUpdateReset: '[configuracion] CONFIGURACION_UPDATE_RESET',
}

export default types
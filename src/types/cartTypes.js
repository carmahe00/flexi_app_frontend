const cartTypes = {
    cartAddItem: '[cart] CART_ADD_ITEM',
    cartRemoveItem: '[cart] CART_REMOVE_ITEM',
    cartSaveShippingAddress: '[cart] CART_SAVE_SHIPPING_ADDRESS',
    cartReset: '[cart] CART_RESET_ITEM_AND_SUGGEST'

}
export default cartTypes;
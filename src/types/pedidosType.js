const types = {
    pedidosPRequest: '[pedido] PEDIDOS_P_REQUEST',
    pedidosPSuccess: '[pedido] PEDIDOS_P_SUCCESS',
    pedidosPFail: '[pedido] PEDIDOS_P_FAIL',
    pedidosPReset: '[pedido] PEDIDOS_P_RESET',

    pedidosDetallePRequest: '[pedidosDetalle] PEDIDOS_DETALLE_P_REQUEST',
    pedidosDetallePSuccess: '[pedidosDetalle] PEDIDOS_DETALLE_P_SUCCESS',
    pedidosDetallePFail: '[pedidosDetalle] PEDIDOS_DETALLE_P_FAIL',
    

    changeStatePeiddoDetalleRequest: '[pedido] PEDIDO_DETALLE_STATE_REQUEST',
    changeStatePeiddoDetalleSuccess: '[pedido] PEDIDO_DETALLE_STATE_SUCCESS',
    changeStatePeiddoDetalleFail: '[pedido] PEDIDO_DETALLE_STATE_FAIL'
}

export default types
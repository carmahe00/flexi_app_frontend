const types = {
    userLoginRequest: '[user] USER_LOGIN_REQUEST',
    userLoginSuccess: '[user] USER_LOGIN_SUCCESS',
    userLoginFail: '[user] USER_LOGIN_FAIL',
    userLogout: '[user] USER_LOGOUT',
    userRenewToken: '[user] USER_RENEW',

    userRegisterRequest: '[user] USER_REGISTER_REQUEST',
    userRegisterSuccess: '[user] USER_REGISTER_SUCCESS',
    userRegisterFail: '[user] USER_REGISTER_FAIL',
    userRegisterReset: '[user] USER_REGISTER_RESET',

    userRecoveryPasswordRequest: '[user] USER_RECOVERY_PASSWORD_REQUEST',
    userRecoveryPasswordSuccess: '[user] USER_RECOVERY_PASSWORD_SUCCESS',
    userRecoveryPasswordFail: '[user] USER_RECOVERY_PASSWORD_FAIL'
}


export default types;
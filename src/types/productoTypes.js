const types = {
    productoListRequest: '[producto] PRODUCTO_LIST_REQUEST',
    productoListSuccess: '[producto] PRODUCTO_LIST_SUCCESS',
    productoListFail: '[producto] PRODUCTO_LIST_FAIL',

    productoDetailsSRequest: '[producto] PRODUCTO_DETAILS_REQUEST',
    productoDetailSuccess: '[producto] PRODUCTO_DETAILS_SUCCESS',
    productoDetailsFail: '[producto] PRODUCTO_DETAILS_FAIL',

    productoDeletesSRequest: '[producto] PRODUCTO_DELETE_REQUEST',
    productoDeleteSuccess: '[producto] PRODUCTO_DELETE_SUCCESS',
    productoDeletesFail: '[producto] PRODUCTO_DELETE_FAIL',


    productoCreateRequest: '[producto] PRODUCTO_CREATE_REQUEST',
    productoCreatSuccess: '[producto] PRODUCTO_CREATE_SUCCESS',
    productoCreateFail: '[producto] PRODUCTO_CREATE_FAIL',
    productoCreateReset: '[producto] PRODUCTO_CREATE_RESET',

    productoUpdateRequest: '[producto] PRODUCTO_UPDATE_REQUEST',
    productoUpdateSuccess: '[producto] PRODUCTO_UPDATE_SUCCESS',
    productoUpdateFail: '[producto] PRODUCTO_UPDATE_FAIL',
    productoUpdateReset: '[producto] PRODUCTO_UPDATE_RESET',

    
    productoPublicListRequest: '[productoPublic] PRODUCTO_PUBLIC_LIST_REQUEST',
    productoPublicListSuccess: '[productoPublic] PRODUCTO_PUBLIC_LIST_SUCCESS',
    productoPublicListFail: '[productoPublic] PRODUCTO_PUBLIC_LIST_FAIL',
}

export default types
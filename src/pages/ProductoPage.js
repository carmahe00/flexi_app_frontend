import React, { useEffect } from 'react'
import { CircularProgress, Container, Grid, Button } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { Add } from '@material-ui/icons';
import { Alert } from '@material-ui/lab'

import { listProductos } from '../actions/productoActions'
import types from '../types/productoTypes'
import { CardProducto } from '../components/CardProducto'
import { ModalProducto } from '../components/ModalProducto'
import { createProducto } from '../actions/productoActions'
import { openModalProducto } from '../actions/modalActions';

export const ProductoPage = ({ location: { state: { params } } }) => {
    const dispatch = useDispatch()
    const { productos, loading, error: errorList } = useSelector(state => state.productoList)
    const { producto, success: successProducto, error: errorCreate } = useSelector(state => state.productoCreate)

    useEffect(() => {
        dispatch({ type: types.productoCreateRequest })
        if (successProducto)
            dispatch(openModalProducto(producto))

        dispatch(listProductos(params.idCategoria))
    }, [dispatch, params, producto, successProducto])

    const createProductHandler = () => {
        dispatch(createProducto(params.idCategoria))
    }

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />

    if (errorList)
        <Alert severity="error" > Error al listar las categorias </Alert>
    if (errorCreate)
        <Alert severity="error" > Error al crear producto </Alert>
    return (
        <>
            <Container>
                <Button
                    variant="contained"
                    color="primary"

                    startIcon={<Add />}
                    disableRipple
                    disableFocusRipple
                    onClick={createProductHandler}
                />
                <Grid container spacing={3}  >
                    {(productos
                    ).map((producto, index) => (
                        <CardProducto producto={producto} key={index} />
                    ))}
                </Grid>
            </Container>
            <ModalProducto categorias={params.categorias} />
        </>
    )
}

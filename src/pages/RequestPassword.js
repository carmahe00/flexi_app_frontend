import React, { useEffect, useState } from 'react'
import * as yup from 'yup';
import { Grid, Avatar, TextField, Button, CircularProgress, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Link } from 'react-router-dom'
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { Alert } from '@material-ui/lab';
import { requestChangePassword } from '../actions/userActions';

const validationSchema = yup.object({
    password: yup
        .string('Enter your password')
        .min(4, 'La contraseña es minima de 4 caracteres')
        .required('Password es obligatorio'),
    confirmPassword: yup
        .string('Enter your password')
        .min(4, 'La contraseña es minima de 4 caracteres')
        .required('Password es obligatorio')
        .oneOf([yup.ref("password"), null], "Contraseña debe coincidir")

});

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, height: '70vh', width: 280, margin: "20px auto"
    },
    avatarStyle: {
        backgroundColor: '#1bbd7e'
    },
    btnStyle: {
        margin: '8px 0'
    },
}))

export const RequestPassword = ({ match, history }) => {
    const classes = useStyles();
    const { loading, error, userInfo } = useSelector(state => state.userRecovery)
    const dispatch = useDispatch()
    const [msg, setMsg] = useState('')

    useEffect(() => {
        if (userInfo) {
            setMsg(userInfo.msg)
            history.push('/')
        }
    }, [userInfo, history])

    const formik = useFormik({
        initialValues: {
            password: '',
            confirmPassword: ''
        },
        validationSchema: validationSchema,
        onSubmit: ({ password, confirmPassword }) => {
            if (password === confirmPassword)
                dispatch(requestChangePassword(match.params.id, match.params.token, password))
        },
    });

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="secondary" />

    return (
        <Grid >

            {
                error && <Alert severity="error"> {error} </Alert>
            }
            {
                msg && <Alert severity="success" >{msg}</Alert>
            }
            <Grid item className={classes.color} >
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid align='center'>
                        <Avatar className={classes.avatarStyle}><LockOutlinedIcon /></Avatar>
                        <h2>Nueva contraseña</h2>
                    </Grid>
                    <form onSubmit={formik.handleSubmit} >
                        <TextField fullWidth
                            type="password"
                            label='password'
                            placeholder='Ingrese su nueva contraseña'
                            name="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                        />
                        <TextField fullWidth
                            type="password"
                            label='Confrimar Password'
                            placeholder='Confirme su nueva contraseña'
                            name="confirmPassword"
                            value={formik.values.confirmPassword}
                            onChange={formik.handleChange}
                            error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                            helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                        />

                        <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} fullWidth>Cambiar Contraseña</Button>

                        <Button component={Link} style={{ borderColor: "white", color: "blue" }} to="/" variant="outlined" >
                            Iniciar Sesión
                        </Button>

                    </form>

                </Paper>
            </Grid>
        </Grid>

    )
}

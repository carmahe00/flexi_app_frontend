import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Grid, Card, CardActionArea, CardContent, CardMedia } from '@material-ui/core';

import deliveryLogo from '../assets/food-delivery.svg'
import administartorLogo from '../assets/web-administrator.svg'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,

    },
    cardContainer: {
        maxWidth: 345,
    },
    media: {
        height: "100%",
        paddingTop: '56.25%', // 16:9
        width: "100%"
    },
}));

export const Home = ({ history }) => {
    const classes = useStyles();
    const hadnleRedirect = (route)=>{
        history.push(route)
    }
    return (
        <div>
            <Grid container className={classes.root} direction="column" alignItems="center" justifyContent="center" style={{ minHeight: "100vh" }} >
                <Grid item xs={12}>
                    <Grid container direction="row" justifyContent="center" spacing={2} alignItems="center" >
                        <Grid item onClick={()=>hadnleRedirect('/home/despachos')} >
                            <Card className={classes.cardContainer}>
                                <CardActionArea>
                                    <CardMedia
                                        className={classes.media}
                                        image={deliveryLogo}
                                        title="Contemplative Reptile"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Despachos
                                    </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            Recibira todos los pedidos realizados por los clientes fuera o dentro del establecimiento.
                                            Se encuentra la información, tiempo transcurrido y cantidad del pedido
                                    </Typography>
                                    </CardContent>
                                </CardActionArea>

                            </Card>
                        </Grid>
                        <Grid item onClick={()=>hadnleRedirect('/administrar/categorias')} >
                            <Card className={classes.cardContainer}>
                                <CardActionArea>
                                    <CardMedia
                                        className={classes.media}
                                        image={administartorLogo}
                                        title="Contemplative Reptile"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Administración
                                    </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            Administra categorias y dentro de ellas sus productos; también, puede agregar una imagen
                                            a cada categoria y producto, si desea que el producto no se vea cambie su categoria a Sin Nombre
                                    </Typography>
                                    </CardContent>
                                </CardActionArea>

                            </Card>
                        </Grid>
                        
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}

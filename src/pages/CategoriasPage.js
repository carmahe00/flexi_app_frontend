import React, { useEffect, useState } from 'react'
import { CircularProgress, Container, Grid, Paper, TablePagination } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Add } from '@material-ui/icons';
import { Alert } from '@material-ui/lab'
import { useDispatch, useSelector } from 'react-redux';

import { TablePaginationActions } from '../components/ui/TablePaginationActions'
import { createCategoria, listCategoria } from '../actions/categoriaActions';
import { CardCategoria } from '../components/CardCategoria';
import { openModalCategoria } from '../actions/modalActions';
import { ModalCategoria } from '../components/ModalCategoria';
import types from '../types/categoriaTypes'

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}));

export const CategoriaPage = () => {
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const { categorias, loading, error: errorList } = useSelector(state => state.categoriaList)
    const { categoria, success: successCreate, error: errorCreate } = useSelector(state => state.categoriaCreate)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({ type: types.categoriaCreateRequest })
        if (successCreate)
            dispatch(openModalCategoria(categoria))
        else
            dispatch(listCategoria(rowsPerPage, page))
    }, [dispatch, page, rowsPerPage, successCreate, categoria])

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const createProductHandler = () => {
        dispatch(createCategoria())
    }

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />

    if (errorList)
        <Alert severity="error" > Error al listar las categorias </Alert>
    if (errorCreate)
        <Alert severity="error" > Error al crear producto </Alert>
    return (
        <>
            <Container >
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<Add />}
                    disableRipple
                    disableFocusRipple
                    onClick={createProductHandler}
                />
                <Grid container spacing={3}  >
                    {(categorias
                    ).map((categoria, index) => (
                        <CardCategoria categoria={categoria} key={index} allCategorias={categorias} />
                    ))}
                </Grid>
                <TablePagination
                    component={Paper}
                    rowsPerPageOptions={[5, 10, 25]}
                    colSpan={3}
                    count={categorias.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                        inputProps: { 'aria-label': 'Filas Por pagina' },
                        native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                />
            </Container>
            <ModalCategoria />
        </>
    )
}

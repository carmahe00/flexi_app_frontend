import React, { useEffect } from 'react'
import { listPedidosP } from '../actions/pedidoPActions'
import { useDispatch, useSelector } from 'react-redux'
import { Alert } from '@material-ui/lab'
import { CircularProgress, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { CardPedido } from '../components/CardPedido';


const useStyles = makeStyles(theme => ({
    alert: {
        width: "100%"
    },
    container: {
        padding: "2em",
        marginBottom: "5em"
    }
}));

export const DespachoPage = () => {
    const classes = useStyles()
    const { loading, pedidoPInfo } = useSelector(state => state.pedidoP)



    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(listPedidosP())

        let timer = setInterval(() => {
            dispatch(listPedidosP())
        }, 5000);
        return () => clearInterval(timer)

    }, [dispatch])

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />

    return (
        <div className={classes.container} >
            <Grid container spacing={3}  >
                {
                    pedidoPInfo.length > 0 ? (pedidoPInfo).map((pedido, index) => (<CardPedido key={index} pedido={pedido} />))
                        : <Alert severity="info" className={classes.alert}> No se han solicitado pedidos </Alert>
                }
            </Grid>


        </div>
    )

}

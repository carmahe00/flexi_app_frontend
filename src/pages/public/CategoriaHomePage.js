import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress, Container, Grid, CardActionArea, Card, Typography, CardMedia } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { Alert } from '@material-ui/lab'

import { listPublicCategoria } from '../../actions/categoriaActions';
import { CardCategoria } from '../../components/public/CardCategoria';
import withoutCategory from '../../assets/category.svg'
import { listConfiguracion } from '../../actions/configuracionActions';
const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    media: {
        height: '100%',
        paddingTop: '5.25%', // 16:9
        [theme.breakpoints.down('sm')]: {
            height: 0,
            paddingTop: '56.25%', // 16:9
        }
    },
    root: {
        width: '100%',
        maxHeight: 130,
        marginBottom: "1em"
    },
    card: {
        position: 'relative',
        height: 130
    },
    overlay: {
        position: 'absolute',
        top: '0px',
        left: '0px',
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0.4)',

        width: '100%',
        height: '100%'
    },
    overlayBotton: {
        position: 'absolute',
        bottom: '0px',
        right: '0px',
        color: 'white',
        width: '100%',

    }
}));

const CategoriaHomePage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { configuraciones } = useSelector(state => state.configuracionList)
    const { categorias, loading, error } = useSelector(state => state.categoriaPublicList)

    useEffect(() => {
        dispatch(listConfiguracion())
        dispatch(listPublicCategoria())
    }, [dispatch])

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />
    if (error)
        <Alert severity="error" > {error} </Alert>
    return (
        <>
            {configuraciones.map((configuracion, index) => (

                <Grid item xs={12} key={index} >
                    <Card className={classes.root} >
                        <CardActionArea className={classes.card} >
                            {
                                (configuracion.imagen && configuracion.imagen !== '')
                                    ? <CardMedia
                                        className={classes.media}
                                        alt="Imagen Portada"

                                        image={`${baseUrl}${configuracion.imagen}`}
                                        title="Imagen Portada"
                                    /> : <CardMedia
                                        className={classes.media}

                                        image={withoutCategory}
                                        title="Imagen Portada"
                                    />
                            }
                            <div className={classes.overlay} >
                                <Typography gutterBottom variant="h5" align="center" >
                                    {configuracion.direccion}
                                </Typography>
                            </div>
                            <div className={classes.overlayBotton} >
                                <Typography gutterBottom variant="h5" align="center">
                                    {configuracion.telefono}
                                </Typography>
                            </div>
                        </CardActionArea>

                    </Card>
                </Grid>


            ))}
            <Container >
                <Grid container spacing={3} justifyContent="center" >
                    {(categorias
                    ).map((categoria, index) => (
                        <CardCategoria categoria={categoria} key={index} allCategorias={categorias} />
                    ))}
                </Grid>
            </Container>
        </>
    )
}

export default CategoriaHomePage
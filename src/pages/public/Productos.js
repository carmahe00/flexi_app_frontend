import React, { useEffect } from 'react'
import { CircularProgress, Container, Grid, useMediaQuery } from '@material-ui/core'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { useDispatch, useSelector } from 'react-redux'
import { Alert } from '@material-ui/lab'

import { listPublicProductos } from '../../actions/productoActions'
import { CardProducto } from '../../components/public/CardProducto'
import { Cart } from '../../components/public/Cart'
import { ModalProductoDetails } from '../../components/public/ModalProductoDetails'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginBottom: "3em",
        marginTop: "1em"
    },

}))

const Productos = ({ location: { state } }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { productos, loading, error } = useSelector(state => state.productoPublicList)
    const theme = useTheme()
    const matchesSM = useMediaQuery(theme.breakpoints.down("sm"))

    useEffect(() => {
        dispatch(listPublicProductos(state.idCategoria))
    }, [dispatch, state])

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />
    if (error)
        <Alert severity="error" > {error} </Alert>
    return (
        <Container>
            <Grid container className={classes.root} spacing={3} >
                <Grid item xs={!matchesSM ? 9 : 12} >
                    <Grid container justifyContent="center" spacing={2}>
                        {(productos
                        ).map((producto, index) => (
                            <CardProducto producto={producto} key={index} />
                        ))}
                    </Grid>
                </Grid>
                {
                    !matchesSM && (
                        <Grid item xs={3} >
                            <Cart />
                        </Grid>
                    )

                }
            </Grid>
            <ModalProductoDetails />
        </Container>
    )
}

export default Productos
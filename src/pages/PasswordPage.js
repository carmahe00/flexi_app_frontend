import React from 'react'
import { makeStyles } from '@material-ui/styles';
import { Button, CircularProgress, Grid, Paper, TextField, Typography } from '@material-ui/core';
import * as yup from 'yup';
import { useFormik } from 'formik';

import { requestChangePassword } from '../actions/userActions';
import { useDispatch, useSelector } from 'react-redux';
import { Alert } from '@material-ui/lab';

const validationSchema = yup.object({
    password: yup
        .string('Enter your password')
        .min(4, 'La contraseña es minima de 4 caracteres')
        .required('Password es obligatorio'),
    confirmPassword: yup
        .string('Enter your password')
        .min(4, 'La contraseña es minima de 4 caracteres')
        .required('Password es obligatorio')
        .oneOf([yup.ref("password"), null], "Contraseña debe coincidir")

});

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '25vh',
        marginTop: '25vh'
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },


}));

export const PasswordPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const { userInfo } = useSelector(state => state.userLogin)
    const { loading, error, userInfo: userInfoPassword } = useSelector(state => state.userRecovery)

    const formik = useFormik({
        initialValues: {
            password: '',
            confirmPassword: ''
        },
        validationSchema: validationSchema,
        onSubmit: ({ password, confirmPassword }) => {
            if (password === confirmPassword)
                dispatch(requestChangePassword(userInfo.soul_usuario.uuid, userInfo.token, password))
        },
    });

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="secondary" />

    return (
        <>
            {
                error && <Alert severity="error"> {error} </Alert>
            }
            {
                userInfoPassword?.msg && <Alert severity="success" >{userInfoPassword.msg}</Alert>
            }
            <div className={classes.root}>

                <Paper className={classes.paper}>
                    <Grid container spacing={2}>

                        <Grid item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <Typography gutterBottom variant="subtitle1">
                                        CAMBIO DE CONTRASEÑA
                                </Typography>
                                    <form onSubmit={formik.handleSubmit} >
                                        <TextField fullWidth
                                            type="password"
                                            label='password'
                                            placeholder='Ingrese su nueva contraseña'
                                            name="password"
                                            value={formik.values.password}
                                            onChange={formik.handleChange}
                                            error={formik.touched.password && Boolean(formik.errors.password)}
                                            helperText={formik.touched.password && formik.errors.password}
                                        />
                                        <TextField fullWidth
                                            type="password"
                                            label='Confrimar Password'
                                            placeholder='Confirme su nueva contraseña'
                                            name="confirmPassword"
                                            value={formik.values.confirmPassword}
                                            onChange={formik.handleChange}
                                            error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                                            helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                                        />

                                        <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} fullWidth>Cambiar Contraseña</Button>


                                    </form>
                                </Grid>

                            </Grid>

                        </Grid>
                    </Grid>
                </Paper>
            </div>
        </>
    )
}

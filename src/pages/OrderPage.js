import React, { useEffect, useState } from 'react'
import { Grid, IconButton, Typography, Grow, FormControl, InputLabel, Input, Select, AppBar, Tabs, Tab, TextField, InputAdornment, MenuItem, Button } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import { AccountCircle, LocationOn, PhoneAndroid, RemoveShoppingCart, Send } from '@material-ui/icons';
import { Alert } from '@material-ui/lab'
import { Phone, EventSeat } from '@material-ui/icons'

import { removeFromCart, saveShippingAddress } from '../actions/cartActions';
import { listPuestos } from '../actions/puestoActions';
import { TabPanel } from '../components/public/TabPanel';
import { Formik } from 'formik';
import { createPedido } from '../actions/pedidoActions';

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: "3em",
        marginTop: "1em"
    },
    header: {
        borderBottom: '.1rem #c0c0c0 solid',
        maxHeight: 'auto',
        overflow: 'hidden',
        overflowY: 'scroll'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    cartItems: {
        padding: 0,
        width: '100%',
        listStyleType: 'none'
    },
    img: {
        width: '5rem'
    },
    rootLocate: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    rootForm: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));
export const OrderPage = ({ history }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { cartItems, shippingAddress } = useSelector(state => state.cart)
    const { error: errorPedido } = useSelector(state => state.pedidoCreate)
    const { puestos, error } = useSelector(state => state.puestoList)
    const [value, setValue] = useState(0)
    const removeItem = (id) => {
        dispatch(removeFromCart(id))
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const a11yProps = (index) => {
        return {
            id: `scrollable-force-tab-${index}`,
            'aria-controls': `scrollable-force-tabpanel-${index}`,
        };
    }

    useEffect(() => {
        dispatch(listPuestos())
    }, [dispatch])


    return (
        <Grid container className={classes.root} spacing={2}
            direction="row"
            justifyContent="center"
            alignItems="center" >
            <Grid item xs={10} sm={8} md={6} >
                {
                    errorPedido && <Alert severity="error" >{errorPedido}</Alert>
                }
                <Grid className={classes.cart} >
                    {
                        cartItems.length === 0 ? <div className={classes.header} ><Typography align="center" >El carro esta vacio</Typography></div>
                            : <div className={classes.header} ><Typography align="center" >Tienes {cartItems.length} productos seleccionados </Typography></div>
                    }
                </Grid>
                <Grid className={classes.header} >
                    {cartItems.map((item, index) => (
                        <Grow in={true} key={index} >
                            <Grid
                                container
                                direction="row"
                                justifyContent="space-between"
                                alignItems="center"
                                spacing={1}>
                                <Grid item >
                                    <img src={`${baseUrl}${item.image}`} alt={item.nombre} className={classes.img} />
                                </Grid>
                                <Grid item >
                                    <Grid item ><Typography color="textSecondary" >{item.nombre}</Typography></Grid >
                                    <Typography color="textSecondary" >{new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(item.precio1)} * {item.count}</Typography>
                                </Grid>
                                <Grid item>
                                    <IconButton onClick={() => removeItem(item.id_cobro)} >
                                        <RemoveShoppingCart color="primary" />
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </Grow>
                    ))}

                </Grid>
                {
                    cartItems.length !== 0 && (

                        <Formik
                            onSubmit={detalles => {
                                if (detalles.id_servicio === 6)
                                    dispatch(saveShippingAddress(detalles))
                                dispatch(createPedido({ detalles, cartItems }))
                                history.push(`/`)
                            }}
                            initialValues={{
                                id_servicio: 6,
                                telefono: shippingAddress.telefono ? shippingAddress.telefono: '',
                                direccion: shippingAddress.direccion ? shippingAddress.direccion: '',
                                nombre: shippingAddress.nombre ? shippingAddress.nombre: '',
                                id_puesto: 0
                            }}
                        >
                            {props => (
                                <form onSubmit={props.handleSubmit}>
                                    <Grid className={classes.cart} >
                                        <Grid className={classes.header}
                                            container
                                            direction="row"
                                            justifyContent="space-between"
                                            alignItems="center"
                                        >

                                            <Grid item className={classes.rootLocate} xs >
                                                <AppBar position="static">
                                                    <Tabs
                                                        value={value}
                                                        onChange={handleChange}
                                                        scrollButtons="on"
                                                        variant="fullWidth"
                                                    >
                                                        <Tab label="¿Fuera del restaurante?" onClick={() => props.setFieldValue('id_servicio', 6)} icon={<Phone />} {...a11yProps(0)} />
                                                        <Tab label="¿Dentro del restaurante?" onClick={() => props.setFieldValue('id_servicio', 5)} icon={<EventSeat />} {...a11yProps(1)} />
                                                    </Tabs>
                                                    <TabPanel value={value} index={0}>
                                                        <TextField
                                                            label='Telefono'
                                                            placeholder='Ingrese su telefono'
                                                            name="telefono"
                                                            value={props.values.telefono}
                                                            onChange={props.handleChange}
                                                            error={props.touched.telefono && Boolean(props.errors.telefono)}
                                                            helperText={props.touched.telefono && props.errors.telefono}
                                                            InputProps={{
                                                                startAdornment: (
                                                                    <InputAdornment position="start">
                                                                        <PhoneAndroid />
                                                                    </InputAdornment>
                                                                ),
                                                            }}
                                                            fullWidth
                                                        />
                                                        <TextField
                                                            label='Direccion'
                                                            placeholder='Ingrese su dirección'
                                                            name="direccion"
                                                            value={props.values.direccion}
                                                            onChange={props.handleChange}
                                                            error={props.touched.direccion && Boolean(props.errors.direccion)}
                                                            helperText={props.touched.direccion && props.errors.direccion}
                                                            InputProps={{
                                                                startAdornment: (
                                                                    <InputAdornment position="start">
                                                                        <LocationOn />
                                                                    </InputAdornment>
                                                                ),
                                                            }}
                                                            fullWidth
                                                            multiline
                                                            maxRows={4}
                                                            minRows={3}
                                                        />
                                                        <TextField
                                                            label='Nombre'
                                                            placeholder='Ingrese su nombre'
                                                            name="nombre"
                                                            value={props.values.nombre}
                                                            onChange={props.handleChange}
                                                            error={props.touched.nombre && Boolean(props.errors.nombre)}
                                                            helperText={props.touched.nombre && props.errors.nombre}
                                                            InputProps={{
                                                                startAdornment: (
                                                                    <InputAdornment position="start">
                                                                        <AccountCircle />
                                                                    </InputAdornment>
                                                                ),
                                                            }}
                                                            fullWidth
                                                        />
                                                    </TabPanel>
                                                    <TabPanel value={value} index={1}>
                                                        {
                                                            error
                                                                ? <Alert severity="error" > {error} </Alert>
                                                                : <FormControl className={classes.formControl}>
                                                                    <InputLabel htmlFor="id_puesto">Mesa</InputLabel>
                                                                    <Select
                                                                        labelId="puesto"

                                                                        input={<Input />}
                                                                        label="Mesa"
                                                                        name="id_puesto"
                                                                        defaultValue={'Mesa'}
                                                                        value={props.values.id_puesto || "Mesa"}
                                                                        onChange={props.handleChange}
                                                                    >
                                                                        <MenuItem value="Mesa">
                                                                            Eliga una mesa
                                                                        </MenuItem>
                                                                        {
                                                                            puestos.map((puesto, index) => (
                                                                                <MenuItem value={puesto.id_puesto} key={index}>{puesto.Nombre}</MenuItem>
                                                                            ))
                                                                        }

                                                                    </Select>
                                                                </FormControl>
                                                        }
                                                    </TabPanel>
                                                </AppBar>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    {
                                        cartItems.length !== 0 && (
                                            <Grid item container
                                                direction="row"
                                                justifyContent="flex-end"
                                                alignItems="center"
                                            >



                                                <Button
                                                    variant="contained"
                                                    color="secondary"
                                                    endIcon={<Send />}
                                                    type="submit"
                                                    disabled={(props.values.id_servicio === 6 && props.values.direccion === '' && props.values.nombre === '' && props.values.telefono === '') ? true : false || (props.values.id_servicio === 5 && props.values.id_puesto === 'Puesto') ? true : false}
                                                >
                                                    {new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(cartItems.reduce((a, c) => a + c.precio1 * c.count, 0))}
                                                </Button>
                                            </Grid>
                                        )
                                    }
                                </form>
                            )}
                        </Formik>

                    )
                }

            </Grid>
        </Grid >
    )
}

import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { listConfiguracion } from '../actions/configuracionActions'
import { Grid, CardActionArea, CardMedia, Typography, Card } from '@material-ui/core'

import withoutCategory from '../assets/category.svg'
import types from '../types/configuracionType'
import { openModalConfiguracion } from '../actions/modalActions';
import { ModalConfiguracion } from '../components/ModalConfiguracion';

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    media: {
        height: '100%',
        paddingTop: '5.25%', // 16:9
        [theme.breakpoints.down('sm')]: {
            height: 0,
            paddingTop: '56.25%', // 16:9
        }
    },
    root: {
        width: '100%',
        maxHeight: 130
    },
    card: {
        position: 'relative',
        height: 130
    },
    overlay: {
        position: 'absolute',
        top: '0px',
        left: '0px',
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0.4)',

        width: '100%',
        height: '100%'
    },
    overlayBotton: {
        position: 'absolute',
        bottom: '0px',
        right: '0px',
        color: 'white',
        width: '100%',

    }
}));

export const ConfiguracionPage = () => {
    const dispatch = useDispatch()
    const { configuraciones } = useSelector(state => state.configuracionList)

    const classes = useStyles();
    useEffect(() => {
        dispatch({ type: types.configuracionCreateRequest })
        dispatch(listConfiguracion())
    }, [dispatch])
    const openModal = (configuracion) => {
        dispatch(openModalConfiguracion(configuracion))
    }
    return (
        <>
            {configuraciones.map((configuracion, index) => (

                <Grid item xs={12} key={index} >
                    <Card className={classes.root} onClick={() => openModal(configuracion)} >
                        <CardActionArea className={classes.card} >
                            {
                                (configuracion.imagen && configuracion.imagen !== '')
                                    ? <CardMedia
                                        className={classes.media}
                                        alt="Imagen Portada"

                                        image={`${baseUrl}${configuracion.imagen}`}
                                        title="Imagen Portada"
                                    /> : <CardMedia
                                        className={classes.media}

                                        image={withoutCategory}
                                        title="Imagen Portada"
                                    />
                            }
                            <div className={classes.overlay} >
                                <Typography gutterBottom variant="h5" align="center" >
                                    {configuracion.direccion}
                                </Typography>
                            </div>
                            <div className={classes.overlayBotton} >
                                <Typography gutterBottom variant="h5" align="center">
                                    {configuracion.telefono}
                                </Typography>
                            </div>
                        </CardActionArea>

                    </Card>
                </Grid>


            ))}
            <ModalConfiguracion />
        </>
    )
}

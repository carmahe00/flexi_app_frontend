import React from 'react'
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Grid, Paper, Avatar, TextField, Button, CircularProgress, FormControlLabel, Checkbox } from '@material-ui/core'
import { useFormik } from 'formik';
import { Alert } from '@material-ui/lab'
import { makeStyles } from '@material-ui/styles'
import { Link } from 'react-router-dom'

import { login } from '../actions/userActions';

const validationSchema = yup.object({
    email: yup
        .string('Enter your email')
        .email('Ingrese un correo valido')
        .required('Email es obligatorio'),
    password: yup
        .string('Enter your password')
        .min(4, 'La contraseña es minima de 4 caracteres')
        .required('Password es obligatorio'),
});

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, height: '70vh', width: 280, margin: "20px auto"
    },
    avatarStyle: {
        backgroundColor: '#1bbd7e'
    },
    btnStyle: {
        margin: '8px 0'
    },
}))

export const LoginPage = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading, error } = useSelector(state => state.userLogin)


    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: ({ email, password }) => {
            dispatch(login(email, password))
        },
    });



    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />


    return (

        <Grid  >
            {
                Array.isArray(error)
                    ? error.map((errorDetail, index) => (
                        <Alert severity="error" key={index} > Email o contraseña invalidos </Alert>
                    ))
                    : error && <Alert severity="error" > {error} </Alert>
            }
            <Grid item className={classes.color} >
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid align='center'>
                        <Avatar className={classes.avatarStyle}><LockOutlinedIcon /></Avatar>
                        <h2>Iniciar Sesión</h2>
                    </Grid>
                    <form onSubmit={formik.handleSubmit} >
                        <TextField fullWidth
                            label='Email'
                            placeholder='Ingrese su correo'
                            name="email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        />
                        <TextField
                            fullWidth
                            placeholder='Enter password'
                            id="password"
                            name="password"
                            label="Password"
                            type="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="checkedB"
                                    color="primary"
                                />
                            }
                            label="Remember me"
                        />
                        <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} fullWidth>Sign in</Button>

                        <Button component={Link} style={{ borderColor: "white", color: "blue" }} to="/reset-password" variant="outlined" >
                            ¿Olvidaste la contraseña ?
                                </Button>

                    </form>

                </Paper>
            </Grid>
        </Grid>

    )
}

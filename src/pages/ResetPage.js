import React, { useEffect, useState } from 'react'
import * as yup from 'yup';
import { Grid, Paper, Avatar, TextField, Button, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useFormik } from 'formik'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Alert } from '@material-ui/lab';
import { map } from 'lodash';

import { recoverPassword } from '../actions/userActions'

const validationSchema = yup.object({
    email: yup
        .string('Enter your email')
        .email('Ingrese un correo valido')
        .required('Email es obligatorio')

});

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, height: '70vh', width: 280, margin: "20px auto"
    },
    avatarStyle: {
        backgroundColor: '#1bbd7e'
    },
    btnStyle: {
        margin: '8px 0'
    },
}))

export const ResetPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading, error, userInfo } = useSelector(state => state.userRecovery)
    const [msg, setMsg] = useState('')

    useEffect(() => {
        if (userInfo)
            setMsg(userInfo.msg)
    }, [userInfo])

    const formik = useFormik({
        initialValues: {
            email: ''
        },
        validationSchema: validationSchema,
        onSubmit: ({ email }) => {

            dispatch(recoverPassword(email))
        },
    });

    if (loading)
        return <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="secondary" />

    return (
        <Grid >
            {map(error, (message, index) => (
                <Alert key={index} severity="error"> {message.msg} </Alert>
            ))}
            {
                msg !== '' && <Alert severity="success" > {msg} </Alert>
            }
            <Grid item className={classes.color} >
                <Paper elevation={10} className={classes.paperStyle}>
                    <Grid align='center'>
                        <Avatar className={classes.avatarStyle}><LockOutlinedIcon /></Avatar>
                        <h2>Recuperar Contraseña</h2>
                    </Grid>
                    <form onSubmit={formik.handleSubmit} >
                        <TextField fullWidth
                            label='Email'
                            placeholder='Ingrese su correo'
                            name="email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        />


                        <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} fullWidth>Recuperar</Button>

                        <Button component={Link} style={{ borderColor: "white", color: "blue" }} to="/" variant="outlined" >
                            Iniciar Sesión
                        </Button>

                    </form>

                </Paper>
            </Grid>
        </Grid>
    )
}

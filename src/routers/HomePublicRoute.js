import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import Productos from '../pages/public/Productos'
import CategoriaHomePage from '../pages/public/CategoriaHomePage'
import Header from '../components/ui/public/Header'
import { Footer } from '../components/ui/Footer'
import { OrderPage } from '../pages/OrderPage'

export const HomePublicPage = () => {
    return (
        <>
            <Header />
            <Switch>


                <Route path="/productos/:categoriaNombre" component={Productos} />
                <Route path="/page-order" component={OrderPage} />
                <Route exact path="/" component={CategoriaHomePage} />

                <Redirect to='/' />
            </Switch>
            <Footer />
        </>
    )
}

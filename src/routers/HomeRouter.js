import React from 'react'
import { Switch, Route } from 'react-router-dom'

import { CartaPage } from '../pages/CartaPage'
import { DespachoPage } from '../pages/DespachoPage'
import { Home } from '../pages/Home'
import { ProductoPage } from '../pages/ProductoPage'

export const HomeRoute = () => {
    return (
        <Switch>
            <Route exact path="/home/despachos" component={DespachoPage}  />
            <Route exact path="/home/administracion" component={ProductoPage}  />
            <Route exact path="/home/carta" component={CartaPage}  />
            

            <Route exact path="/home" component={Home}  />
        </Switch>
    )
}

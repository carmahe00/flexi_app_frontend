import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {
    BrowserRouter as Router,
    Switch
} from "react-router-dom";

import { LoginPage } from '../pages/LoginPage';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

import { AdministrarRoute } from './AdministrarRoute';
import { PasswordPage } from '../pages/PasswordPage';
import { ResetPage } from '../pages/ResetPage';
import { RequestPassword } from '../pages/RequestPassword';
import { HomeRoute } from './HomeRouter';
import { renewToken } from '../actions/userActions'
import { HomePublicPage } from './HomePublicRoute';

const AppRouter = () => {
    const dispatch = useDispatch();
    const { userInfo } = useSelector(state => state.userLogin)

    useEffect(() => {
        if (!!userInfo)
            dispatch(renewToken())
    }, [dispatch, userInfo])

    return (
        <Router>
            <Switch>
                
                <PublicRoute path="/login" component={LoginPage} exact isAuthenticated={!userInfo} />
                <PublicRoute path="/reset-password" component={ResetPage} exact isAuthenticated={!userInfo} />
                <PublicRoute path="/password-reset/:id/:token" component={RequestPassword} exact isAuthenticated={!userInfo} />

                <PrivateRoute path='/home' component={HomeRoute} isAuthenticated={!!userInfo} />
                <PrivateRoute path='/administrar' component={AdministrarRoute} isAuthenticated={!!userInfo} />
                <PrivateRoute path='/usuario/contrasena' component={PasswordPage} isAuthenticated={!!userInfo} />

                <PublicRoute path="/" component={HomePublicPage} isAuthenticated={!userInfo} />
                
            </Switch>
        </Router>
    )
}

export default AppRouter

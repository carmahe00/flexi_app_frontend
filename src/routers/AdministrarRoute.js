import React from 'react'
import { Switch, Route } from 'react-router-dom'

import { AdministrarPage } from '../pages/AdministrarPage'
import { CategoriaPage } from '../pages/CategoriasPage'
import { ConfiguracionPage } from '../pages/ConfiguracionPage'
import { ProductoPage } from '../pages/ProductoPage'

export const AdministrarRoute = () => {
    return (
        <Switch>
            <Route exact path="/administrar/categorias" component={CategoriaPage}  />
            <Route exact path="/administrar/categorias/:nombreCategoria" component={ProductoPage}  />
            <Route exact path="/administrar/configuracion" component={ConfiguracionPage}  />

            <Route exact path="/administrar" component={AdministrarPage}  />
        </Switch>
    )
}

import React, { useState } from 'react'
import axios from 'axios';
import * as yup from 'yup';
import { Formik } from 'formik';
import { makeStyles } from '@material-ui/styles';
import { PhotoCamera } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import { Modal, Paper, TextField, Button, IconButton, CircularProgress } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import { closeModalConfiguracion } from '../actions/modalActions';
import { updateConfiguracion } from '../actions/configuracionActions';

const baseUrl = process.env.REACT_APP_API_URL

const validationSchema = yup.object({
    Nit: yup.string('Ingrese el codigo')
        .required('El codigo es obligatorio'),
    nombre: yup
        .string('Ingrese el nombre')
        .required('Nombre es obligatorio'),
    telefono: yup
        .number('Solo Numeros')
        .required('Telefono es requerido'),
    direccion: yup
        .string('Ingrese una direaccion')
        .required('direccion es requerido'),
    imagen: yup
        .string('Ingrese la imagen')
        .required('imagen es obligatoria'),
})

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, margin: "20px auto",
        maxWidth: 800
    },
    btnStyle: {
        margin: '8px 0'
    },
    input: {
        display: 'none',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    rootForm: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}))
export const ModalConfiguracion = () => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const { userInfo } = useSelector(state => state.userLogin);
    const { configuracionModalOpen, configuracionModal } = useSelector(state => state.modalConfiguracion);
    const [uploading, setUploading] = useState(false);
    const handleClose = () => dispatch(closeModalConfiguracion());

    const uploadFileHanlder = async (e, props) => {
        const file = e.target.files[0]
        const formData = new FormData()
        formData.append('image', file)
        setUploading(true)
        try {
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `${userInfo.token}`
                }
            }
            const { data } = await axios.post(`${baseUrl}/upload/configuracion/${configuracionModal.Nit}`, formData, config)
            props.setFieldValue('imagen', data)
            setUploading(false)
        } catch (error) {
            console.log(error)
            setUploading(false)
        }
    }
    return (
        <Modal
            closeAfterTransition
            open={configuracionModalOpen}
            BackdropProps={{
                timeout: 500
            }}
            onClose={handleClose}
            className={classes.modal}
        >

            <Paper elevation={5} className={classes.paperStyle} >
                <Formik
                    validationSchema={validationSchema}
                    className={classes.rootForm}
                    initialValues={{
                        Nit: configuracionModal.Nit ? configuracionModal.Nit : '',
                        nombre: configuracionModal.nombre ? configuracionModal.nombre : '',
                        direccion: configuracionModal.direccion ? configuracionModal.direccion : '',
                        telefono: configuracionModal.telefono ? configuracionModal.telefono : '',
                        imagen: configuracionModal.imagen ? configuracionModal.imagen : '',
                    }}
                    onSubmit={(props) => {
                        dispatch(updateConfiguracion(props, configuracionModal.Nit))
                        handleClose()
                        history.push('/administrar/configuracion')
                    }}
                >
                    {props => (
                        <form onSubmit={props.handleSubmit} >
                            <TextField
                                label='Nit'
                                placeholder='Ingrese el Nit'
                                name="Nit"
                                value={props.values.Nit}
                                onChange={props.handleChange}
                                error={props.touched.Nit && Boolean(props.errors.Nit)}
                                helperText={props.touched.Nit && props.errors.Nit}
                            />
                            <TextField
                                label='Nombre'
                                placeholder='Ingrese el nombre'
                                name="nombre"
                                value={props.values.nombre}
                                onChange={props.handleChange}
                                error={props.touched.nombre && Boolean(props.errors.nombre)}
                                helperText={props.touched.nombre && props.errors.nombre}
                            />
                            <TextField
                                label='Direccion'
                                placeholder='Ingrese la direccion'
                                name="direccion"
                                value={props.values.direccion}
                                onChange={props.handleChange}
                                error={props.touched.direccion && Boolean(props.errors.direccion)}
                                helperText={props.touched.direccion && props.errors.direccion}
                            />
                            <TextField
                                label='Telefono'
                                placeholder='Ingrese la telefono'
                                name="telefono"
                                value={props.values.telefono}
                                onChange={props.handleChange}
                                error={props.touched.telefono && Boolean(props.errors.telefono)}
                                helperText={props.touched.telefono && props.errors.telefono}
                            />
                            <input accept="image/*" className={classes.input} id="icon-button-file" type="file" onChange={(e) => uploadFileHanlder(e, props)} />
                            <label htmlFor="icon-button-file">
                                <IconButton color="primary" aria-label="upload picture" component="span"  >
                                    <PhotoCamera />
                                </IconButton>
                                {uploading && <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />}
                            </label>
                            <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} >Enviar</Button>
                        </form>
                    )}
                </Formik>
            </Paper>
        </Modal>
    )
}

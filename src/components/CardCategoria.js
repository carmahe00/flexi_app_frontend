import React from 'react'
import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography, CardActions, Button } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

import withoutCategory from '../assets/category.svg'
import { openModalCategoria } from '../actions/modalActions'

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    root: {
        maxWidth: 345,
    },
}));

export const CardCategoria = ({ categoria, allCategorias }) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const openModal = () => {
        dispatch(openModalCategoria(categoria))
    }
    return (
        <Grid item xs={12} sm={3} >
            <Card className={classes.root} >
                <CardActionArea onClick={openModal} >
                    {
                        categoria.image
                            ? <CardMedia
                                className={classes.media}
                                alt="Contemplative Reptile"

                                image={`${baseUrl}${categoria.image}`}
                                title="Contemplative Reptile"
                            /> : <CardMedia
                                className={classes.media}

                                image={withoutCategory}
                                title="Contemplative Reptile"
                            />
                    }
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {categoria.nombre}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {categoria.descripcion}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button component={Link} to={{ pathname: `/administrar/categorias/${categoria.nombre}`, state: { params: { idCategoria: categoria.id_grupo, categorias: allCategorias } } }} size="small" color="primary">
                        Ver productos
                    </Button>
                </CardActions>
            </Card>
        </Grid>
    )
}

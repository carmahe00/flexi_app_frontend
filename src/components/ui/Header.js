import React, { useEffect, useMemo, useState } from 'react'
import { AppBar, useScrollTrigger, Toolbar, Button, Tabs, Tab, Menu, MenuItem, useTheme, useMediaQuery, SwipeableDrawer, List, ListItem, ListItemText, IconButton } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import { Menu as MenuIcon } from '@material-ui/icons';
import { useDispatch } from 'react-redux'

import logo from '../../assets/flexi-header.svg'
import { logout } from '../../actions/userActions';

function ElevationScroll(props) {
    const { children, window } = props

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined
    })

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    })
}

const useStyles = makeStyles(theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar,
        marginBottom: "2em",
        [theme.breakpoints.down("md")]: {
            marginBottom: "1em",
        },
        [theme.breakpoints.down("xs")]: {
            marginBottom: "0.25em",
        }
    },
    tabContainer: {
        marginLeft: 'auto'
    },
    tab: {
        ...theme.typography.tab,
        minWidth: 10,
        marginLeft: "25px"
    },
    appbar: {
        zIndex: theme.zIndex.modal + 1
    },
    logo: {
        height: "6.5em",
        [theme.breakpoints.down("md")]: {
            height: "5.5em"
        },
        [theme.breakpoints.down("xs")]: {
            height: "4.5em"
        }
    },
    button: {
        marginRight: "25px",
        "&:hover": {
            backgroundColor: theme.palette.secondary.light
        }
    },
    logoContainer: {
        padding: 0,
        "&:hover": {
            backgroundColor: "transparent"
        }
    },
    menu: {
        backgroundColor: theme.palette.common.blue,
        color: "white",
        borderRadius: "0px"
    },
    menuItem: {
        ...theme.typography.tab,
        opacity: 0.7,
        "&:hover": {
            opacity: 1
        }
    },
    drawer: {
        backgroundColor: theme.palette.common.blue
    },
    drawerIconContainer: {
        marginLeft: "auto",
        "&:hover": {
            backgroundColor: "transparent"
        }
    },
    drawerIcon: {
        height: "40px",
        width: "40px"
    },
    drawerItem: {
        ...theme.typography.tab,
        opacity: 0.7,
        color: 'white'
    },
    drawerItemSelected: {
        opacity: 1
    },
    drawerItemEstimate: {
        backgroundColor: theme.palette.common.orange
    }
}))

export const Header = ({ value, setValue, selectedIndex, setSelectedIndex, selectedIndexUser, setSelectedIndexUser }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null)
    const [openMenu, setOpenMenu] = useState(false)
    const [openMenuUser, setOpenMenuUser] = useState(false)
    const [anchorElUser, setAnchorElUser] = useState(null)

    const theme = useTheme() // Access default theme
    const matches = useMediaQuery(theme.breakpoints.down('md'));
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
    const [openDrawer, setOpenDrawer] = useState(false)
    const dispatch = useDispatch()

    const menuOptions = useMemo(() => [
        { name: "Administrar", link: "/administrar", activeIndex: 1, selectIndex: 0 },
        { name: "Categorias", link: "/administrar/categorias", activeIndex: 1, selectIndex: 1 },
        { name: "Configuracion", link: "/administrar/configuracion", activeIndex: 1, selectIndex: 2 },
    ], [])




    const routes = useMemo(() => [
        { name: "Home", link: "/home", activeIndex: 0 },
        { name: "Administrar", link: "/administrar", activeIndex: 1, ariaOwns: "simple-menu", ariaPopup: "true", mouseOver: event => handleClick(event) },
        { name: "Configuracion", link: "#", activeIndex: 2, ariaOwns: "simple-menu-user", ariaPopup: "true", mouseOver: event => handleClickUser(event) },

    ], [])

    const routesMatch = useMemo(() => [
        { name: "Home", link: "/home", activeIndex: 0 },
        { name: "Categorias", link: "/administrar/categorias", activeIndex: 1, ariaOwns: "simple-menu", ariaPopup: "true", mouseOver: event => handleClick(event) },
        { name: "Configuracion", link: "/administrar/configuracion", activeIndex: 2, ariaOwns: "simple-menu-user", ariaPopup: "true", mouseOver: event => handleClickUser(event) },

    ], [])

    const handleClick = e => {
        setAnchorEl(e.currentTarget)
        setOpenMenu(true)
    }

    const handleClickUser = e => {
        setAnchorElUser(e.currentTarget)
        setOpenMenuUser(true)
    }
    const handleLogout = () => {
        dispatch(logout())
        window.location.href = '/';
    }

    const home = () => {
        window.location.href = '/';
    }

    const menuOptionsUser = useMemo(() => [

        { name: "Contraseña", link: "/usuario/contrasena", activeIndex: 2, selectIndex: 0 },
        { name: "Salir", activeIndex: 2, selectIndex: 1 },
    ], [])

    useEffect(() => {
        [...menuOptions, ...routes].forEach(route => {
            switch (window.location.pathname) {
                case `${route.link}`:
                    if (value !== route.activeIndex) {
                        setValue(route.activeIndex)
                        if (route.selectIndex && route.selectIndex !== selectedIndex)
                            setSelectedIndex(route.selectIndex)
                    }
                    break;
                default:
                    break;
            }
        })
    }, [selectedIndex, value, routes, menuOptions, setSelectedIndex, setValue])

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const handleMenuItemClick = (e, i) => {
        setAnchorEl(null)
        setOpenMenu(false)
        setSelectedIndex(i)

        setValue(1)
        handleClose()
    }

    const handleMenuItemClickUser = (e, i) => {
        setAnchorElUser(null)
        setOpenMenuUser(false)
        setSelectedIndexUser(i)

        setValue(2)
        handleCloseUser()
    }

    const handleClose = e => {
        setAnchorEl(null)
        setOpenMenu(false)
    }

    const handleCloseUser = e => {
        setAnchorElUser(null)
        setOpenMenuUser(false)
    }

    const drawer = (
        <>
            <SwipeableDrawer
                disableBackdropTransition={!iOS} disableDiscovery={iOS}
                open={openDrawer}
                onClose={() => setOpenDrawer(false)}
                onOpen={() => setOpenDrawer(true)}
                classes={{ paper: classes.drawer }}
            >
                <div className={classes.toolbarMargin} />
                <List disablePadding >
                    {
                        routesMatch.map((route, index) => (
                            <ListItem
                                key={index}
                                divider={index === 0}
                                button
                                component={Link}
                                onClick={() => { setOpenDrawer(false); setValue(route.activeIndex) }}
                                selected={value === index}
                                classes={{ selected: classes.drawerItemSelected }}
                                to={route.link}
                            >

                                <ListItemText disableTypography classes={{ root: classes.drawerItem }} >{route.name}</ListItemText>
                            </ListItem>
                        ))}
                    <ListItem
                        onClick={handleLogout}
                        divider
                        button
                        classes={{ root: classes.drawerItemEstimate, selected: classes.drawerItemSelected }}

                    >
                        <ListItemText classes={{ root: classes.drawerItem }} disableTypography  >Salir</ListItemText>
                    </ListItem>
                </List>
            </SwipeableDrawer>
            <IconButton onClick={() => setOpenDrawer(!openDrawer)} disableRipple className={classes.drawerIconContainer} >
                <MenuIcon className={classes.drawerIcon} />
            </IconButton>
        </>
    )

    const tabs = (
        <>
            <Tabs classes={{ root: classes.tabContainer }} onChange={handleChange} value={value} indicatorColor="secondary" >
                {
                    routes.map((route, index) => (
                        <Tab key={index} className={(routes.length - 1) === index ? classes.button : ''} classes={{ root: classes.tab }} component={Link} to={route.link} label={route.name} aria-owns={route.ariaOwns} aria-haspopup={route.ariaPopup} onMouseOver={route.mouseOver} />
                    ))
                }
            </Tabs>

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={openMenu}
                onClose={handleClose}
                elevation={0}
                MenuListProps={{ onMouseLeave: handleClose }}
                classes={{ paper: classes.menu }}
                keepMounted
                style={{ zIndex: 1302 }}
            >
                {
                    menuOptions.map((option, i) => (
                        <MenuItem
                            key={i}
                            component={Link}
                            to={option.link}
                            onClick={(event) => handleMenuItemClick(event, i)}
                            classes={{ root: classes.menuItem }}
                            selected={i === selectedIndex && value === 1}
                        >
                            {option.name}
                        </MenuItem>
                    ))
                }
            </Menu>
            <Menu
                id="simple-menu-user"
                anchorEl={anchorElUser}
                open={openMenuUser}
                onClose={handleCloseUser}
                MenuListProps={{ onMouseLeave: handleCloseUser }}
                classes={{ paper: classes.menu }}
                keepMounted
                style={{ zIndex: 1302 }}
            >
                {
                    menuOptionsUser.map((option, i) => (
                        option.name !== 'Salir' ? <MenuItem
                            key={i}
                            component={Link}
                            to={option.link}

                            onClick={event => handleMenuItemClickUser(event, i)}
                            classes={{ root: classes.menuItem }}
                            selected={i === selectedIndexUser && value === 2}
                        >
                            {option.name}
                        </MenuItem> :
                            <MenuItem
                                key={i}
                                classes={{ root: classes.menuItem }}
                                onClick={handleLogout}
                            >
                                {option.name}
                            </MenuItem>
                    ))
                }
            </Menu>
        </>
    )

    return (
        <>
            <ElevationScroll>
                <AppBar className={classes.appbar} position="fixed" >
                    <Toolbar disableGutters >
                        <Button component={Link} onClick={home} className={classes.logoContainer} disableRipple >
                            <img alt="Flexi" src={logo} className={classes.logo} />
                        </Button>
                        {matches ? drawer : tabs}
                    </Toolbar>
                </AppBar>
            </ElevationScroll>
            <div className={classes.toolbarMargin} />
        </>
    )
}

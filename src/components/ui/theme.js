import { createTheme } from '@material-ui/core/styles';


const arcBlue = "#0B72B9"
const arcOrange = "#FFBA60"
const arcGrey = "#868686"

export default createTheme({
    palette: {
        common: {
            blue: arcBlue,
            orange: arcOrange
        },
        primary: {
            main: arcBlue
        },
        secondary: {
            main: arcOrange
        }
    },
    typography: {
        tab: {
            fontFamily: "Raleway",
            textTransform: "none",
            fontWeight: 700,
            fontSize: "1rem",
        },
        h2: {
            fontFamily: "Raleway",
            fontWeight: 700,
            fontSize: "2.5rem",
            color: arcBlue,
            lineHeight: 1.5
        },
        h4: {
            fontFamily: "Raleway",
            fontSize: "1.7rem",
            color: arcBlue,
            fontWeight: 700
        },
        h3:{
            fontFamily: "Pacifico",
            fontSize: "2.5rem",
            color: arcBlue
        },
        subtitle1: {
            fontSize: "1.25rem",
            fontWeight: 300,
            color: arcGrey
        },
        subtitle2:{
            color: "white",
            fontSize: "1.25rem",
            fontWeight: 300
        },
        estimate: {
            fontFamily: "Pacifico",
            fontSize: "1rem",
            textTransform: "none",
            color: "white"
        },
        learnButton: {
            borderColor: arcBlue,
            borderWidth: 2,
            color: arcBlue,
            textTransform: "none",
            borderRadius: 50,
            fontFamily: "Roboto",
            fontWeight: "bold"
        }
    }
})

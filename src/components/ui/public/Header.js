import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { AddShoppingCart } from '@material-ui/icons';
import { AppBar, Button, IconButton, Toolbar, Typography, useScrollTrigger } from '@material-ui/core'

import logo from '../../../assets/flexi-header.svg'

function ElevationScroll(props) {
    const { children, window } = props

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined
    })

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    })
}

const useStyles = makeStyles(theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar,
        
    },

    appbar: {
        zIndex: theme.zIndex.modal + 1
    },
    logo: {
        height: "4.5em",
        [theme.breakpoints.down("md")]: {
            height: "3.5em"
        },
        [theme.breakpoints.down("xs")]: {
            height: "2.5em"
        }
    },
    buttonCart: {
        marginLeft: 'auto'
    },
    logoContainer: {
        padding: 0,
        "&:hover": {
            backgroundColor: "transparent"
        }
    },
    menu: {
        backgroundColor: theme.palette.common.blue,
        color: "white",
        borderRadius: "0px"
    },

}))

const Header = () => {
    const classes = useStyles();
    const { cartItems } = useSelector(state => state.cart)
    return (
        <>
            <ElevationScroll>
                <AppBar className={classes.appbar} position="fixed" >
                    <Toolbar disableGutters >
                        <Button component={Link} to="/" className={classes.logoContainer} disableRipple >
                            <img alt="Flexi" src={logo} className={classes.logo} />
                        </Button>

                        <IconButton component={Link} to="/page-order" aria-label="delete" className={classes.buttonCart} >
                            <Typography variant="h4" color="secondary">{new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(cartItems.reduce((a, c) => a + c.precio1 * c.count, 0))}</Typography>
                            <AddShoppingCart color="secondary" fontSize="medium" />
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </ElevationScroll>
            <div className={classes.toolbarMargin} />
        </>
    )
}


export default Header;
import React from 'react'
import { Card, Grid, Typography, CardActionArea, CardMedia } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux'

import withoutCategory from '../../assets/category.svg'
import { openModalProductoPublic } from '../../actions/modalActions';

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    image: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    card: {
        maxWidth: 345,
        height: 190
    },
    cardImage: {
        position: 'relative',
    },
    overlay: {
        position: 'absolute',
        top: '0px',
        left: '0px',
        backgroundColor: 'rgba(0,0,0,0.2)',

        width: '100%',
        height: '100%'
    },
    mainContainer: {
        marginTop: '1vh',
        marginLeft: '1vh'
    }
}));

export const CardProducto = ({ producto }) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const openModal = () => {
        dispatch(openModalProductoPublic(producto))
    }
    return (
        <Grid item xs={6} sm={3} >
            <Card className={classes.card} onClick={openModal} >
                <CardActionArea className={classes.cardImage} >
                    {
                        producto.image
                            ? <CardMedia
                                className={classes.image}
                                alt="Contemplative Reptile"

                                image={`${baseUrl}${producto.image}`}
                                title="Contemplative Reptile"
                            /> : <CardMedia
                                className={classes.image}

                                image={withoutCategory}
                                title="Contemplative Reptile"
                            />
                    }
                    <div className={classes.overlay} ></div>
                </CardActionArea>
                <Grid
                    container
                    direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    className={classes.mainContainer}

                >
                    <Grid item >
                        <Typography variant="body1" gutterBottom >
                            {producto.nombre}
                        </Typography>
                    </Grid>
                    <Grid item >
                        <Typography color="primary" align="left" >
                            ${producto.precio1}
                        </Typography>
                    </Grid>
                </Grid>

            </Card>
        </Grid>
    )
}

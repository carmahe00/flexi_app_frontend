import React from 'react'
import { Box } from '@material-ui/core';

export const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3} style={{ backgroundColor: 'white', color: 'black' }} >
                    {children}
                </Box>
            )}
        </div>
    );
}

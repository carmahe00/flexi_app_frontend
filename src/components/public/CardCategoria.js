import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardActionArea, CardMedia, Grid, Typography } from '@material-ui/core';

import withoutCategory from '../../assets/category.svg'
import { useHistory } from 'react-router-dom';

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9

    },
    root: {
        maxWidth: 345,
        maxHeight: 130
    },
    card: {
        position: 'relative',
    },
    overlay: {
        position: 'absolute',
        top: '0px',
        left: '0px',
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0.4)',
        
        width: '100%',
        height: '100%'
    }
}));

export const CardCategoria = ({ categoria }) => {
    const classes = useStyles();
    const history = useHistory();

    const redirectToProducts = () => {
        history.push({ pathname: `/productos/${categoria.nombre}`, state: { idCategoria: categoria.id_grupo } })
    }

    return (
        <Grid item xs={12} sm={3} >
            <Card className={classes.root} onClick={redirectToProducts} >
                <CardActionArea className={classes.card} >
                    {
                        categoria.image
                            ? <CardMedia
                                className={classes.media}
                                alt="Contemplative Reptile"

                                image={`${baseUrl}${categoria.image}`}
                                title="Contemplative Reptile"
                            /> : <CardMedia
                                className={classes.media}

                                image={withoutCategory}
                                title="Contemplative Reptile"
                            />
                    }
                    <div className={classes.overlay} >
                        <Typography  gutterBottom variant="h5" align="center" >
                            {categoria.nombre}
                        </Typography>
                    </div>

                </CardActionArea>

            </Card>
        </Grid>
    )
}

import React from 'react'
import { Fade, Grid, IconButton, Typography, Grow } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import { RemoveShoppingCart, Send } from '@material-ui/icons';
import { useHistory } from 'react-router-dom'

import { removeFromCart } from '../../actions/cartActions';

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles(({

    header: {
        borderBottom: '.1rem #c0c0c0 solid',
        maxHeight: 150,
        overflow: 'hidden',
        overflowY: 'scroll'
    },
    cartItems: {
        padding: 0,
        width: '100%',
        listStyleType: 'none'
    },
    img: {
        width: '5rem'
    }
}));

export const Cart = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const { cartItems, sugerencias } = useSelector(state => state.cart)
    const removeItem = (id) => {
        dispatch(removeFromCart(id))
    }
    return (
        <Grid item  >
            <Grid className={classes.cart} >
                {
                    cartItems.length === 0 ? <div className={classes.header} ><Typography>El carro esta vacio</Typography></div>
                        : <div className={classes.header} ><Typography>Tienes {cartItems.length} productos seleccinados </Typography></div>
                }
            </Grid>
            <Grid className={classes.header} >
                {cartItems.map((item, index) => (
                    <Grow in={true} key={index} >
                        <Grid 
                            container
                            direction="row"
                            justifyContent="space-between"
                            alignItems="center"
                            spacing={1}>
                            <Grid item >
                                <img src={`${baseUrl}${item.image}`} alt={item.nombre} className={classes.img} />
                            </Grid>
                            <Grid item >
                                <Grid item ><Typography color="textSecondary" >{item.nombre}</Typography></Grid >
                                <Typography color="textSecondary" >{new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(item.precio1)} * {item.count}</Typography>
                            </Grid>
                            <Grid item>
                                <IconButton onClick={() => removeItem(item.id_cobro)} >
                                    <RemoveShoppingCart color="primary" />
                                </IconButton>
                            </Grid>
                        </Grid>
                    </Grow>
                ))}

            </Grid>

            <Grid className={classes.header}  >
                {
                    sugerencias.length !== 0 && (
                        sugerencias.map((item, index) => (
                            <Fade in={true} key={index} >
                                <Grid 
                                    container
                                    direction="row"
                                    justifyContent="space-between"
                                    alignItems="center"
                                    spacing={1}>
                                    <Grid item >
                                        <Typography color="textPrimary" >{item.nombre}</Typography>
                                    </Grid>

                                    <Grid item>
                                        <Typography color="textSecondary" >{item.sugerencia}</Typography>
                                    </Grid>
                                </Grid>
                            </Fade>
                        ))
                    )
                }
            </Grid>
            {
                cartItems.length !== 0 && (
                    <Grid item container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        spacing={1}>

                        <Typography color="primary">Total:{" "}{new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(cartItems.reduce((a, c) => a + c.precio1 * c.count, 0))}</Typography>
                        <IconButton onClick={() => history.push('/page-order')} >
                            <Send color="primary" />
                        </IconButton>
                    </Grid>
                )
            }

        </Grid >
    )
}

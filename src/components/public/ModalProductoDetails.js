import React from 'react'
import { Modal, CardMedia, CardContent, Typography, Card, TextField, InputAdornment, Grid, Button, ButtonGroup } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { Comment, AddShoppingCart } from '@material-ui/icons';
import * as yup from 'yup';

import withoutCategory from '../../assets/category.svg'
import { closeModalProductoPublic } from '../../actions/modalActions'
import { Formik } from 'formik';
import { addToCart } from '../../actions/cartActions';
const baseUrl = process.env.REACT_APP_API_URL_BASE

const validationSchema = yup.object({
    count: yup.number('solo numeros')
        .required().min(1, 'cantidad debe ser mayor a 0'),
    sugerencias: yup.string('solo letras').max(200, 'Máximo 200 caracteres')
})

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    image: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    card: {
        width: 345,
        height: 'auto'
    },
    margin: {
        margin: theme.spacing(1),
    },
    mainContainer: {
        marginTop: '1vh',
        marginLeft: '0.5vh',
        marginRight: '0.5vh',
        marginBottom: 2
    },
    button: {
        color: 'white'
    }
}))

export const ModalProductoDetails = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const { productoPublicModalOpen, productoPublicModal } = useSelector(state => state.modalProductoPublicForm)
    const handleClose = () => dispatch(closeModalProductoPublic());

    return (
        <Modal
            open={productoPublicModalOpen}
            closeAfterTransition
            BackdropProps={{
                timeout: 500
            }}
            onClose={handleClose}
            className={classes.modal}
        >
            <Card className={classes.card} >

                {
                    productoPublicModal.image
                        ? <CardMedia
                            className={classes.image}
                            alt="Contemplative Reptile"

                            image={`${baseUrl}${productoPublicModal.image}`}
                            title="Contemplative Reptile"
                        /> : <CardMedia
                            className={classes.image}

                            image={withoutCategory}
                            title="Contemplative Reptile"
                        />
                }
                <Formik
                    onSubmit={(values, { resetForm }) => {
                        const { count, sugerencias } = values
                        resetForm({ values: '' })
                        dispatch(addToCart({ count, ...productoPublicModal, sugerencias: sugerencias === '' ? 'Normal' : sugerencias }, sugerencias))
                        handleClose()
                    }}
                    validationSchema={validationSchema}
                    initialValues={{
                        count: 1,
                        sugerencias: ''
                    }}
                >
                    {
                        props => (
                            <form onSubmit={props.handleSubmit}>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {productoPublicModal.nombre}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {productoPublicModal.descripcion}
                                    </Typography>
                                    <Typography color="primary" >
                                        {new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(productoPublicModal.precio1)}
                                    </Typography>

                                    <TextField
                                        className={classes.margin}

                                        label="¿Tienes alguna sugereuncia?"
                                        name="sugerencias"
                                        fullWidth
                                        placeholder="Ej: sin tomate, salsa barbacoa aparte, con pepinillo, etc"
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <Comment />
                                                </InputAdornment>
                                            ),
                                        }}
                                        multiline={true}
                                        minRows={2}
                                        value={props.values.sugerencias}
                                        onChange={props.handleChange}
                                        error={props.touched.sugerencias && Boolean(props.errors.sugerencias)}
                                        helperText={props.touched.sugerencias && props.errors.sugerencias}
                                    />

                                </CardContent>
                                <Grid
                                    container
                                    direction="row"
                                    justifyContent="flex-start"
                                    alignItems="flex-start"
                                    className={classes.mainContainer}

                                >
                                    {props.errors.count ? (

                                        <Typography variant="subtitle1" >{props.errors.count}</Typography>

                                    ) : null}
                                    <Grid item xs={5} >

                                        <ButtonGroup size="small" color="primary" aria-label="text primary button group">
                                            <Button type='button' onClick={() => props.setFieldValue('count', props.values.count - 1)}>-</Button>
                                            <Button disabled >{props.values.count}</Button>
                                            <Button type='button' onClick={() => props.setFieldValue('count', props.values.count + 1)}>+</Button>
                                        </ButtonGroup>
                                    </Grid>
                                    <Grid item xs={7} >
                                        <Button
                                            fullWidth
                                            size="small"
                                            variant="contained"
                                            color="secondary"
                                            disableElevation
                                            className={classes.button}
                                            endIcon={<AddShoppingCart />}
                                            type='submit'
                                        >
                                            {new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'COP' }).format(productoPublicModal.precio1 * props.values.count)}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        )
                    }

                </Formik>
            </Card>
        </Modal>
    )
}

import React, { useState } from 'react'
import { Button, CircularProgress, FormControl, IconButton, InputLabel, MenuItem, Modal, Paper, Select, TextField } from '@material-ui/core';
import * as yup from 'yup';
import { Formik } from 'formik';
import { makeStyles } from '@material-ui/styles'
import axios from 'axios';
import { useHistory } from 'react-router-dom';

import { closeModalProducto } from '../actions/modalActions'
import { useSelector, useDispatch } from 'react-redux';
import { PhotoCamera } from '@material-ui/icons';
import { updateproducto } from '../actions/productoActions';
const baseUrl = process.env.REACT_APP_API_URL

const validationSchema = yup.object({
    codigo: yup.number('Ingrese el codigo')
        .required('El codigo es obligatorio'),
    nombre: yup
        .string('Ingrese el nombre')
        .required('Nombre es obligatorio'),
    descripcion: yup
        .string('Ingrese la descripcion')
        .required('descripcion es obligatoria'),
    valor: yup
        .number('Solo Numeros')
        .required('Valor es requerido'),
    total: yup
        .number('Solo Numeros')
        .required('Total es requerido'),
    impuesto: yup
        .number('Solo Numeros')
        .required('Impuesto es requerido'),
    precio1: yup
        .number('Solo Numeros')
        .required('Precio es requerido'),
    inicial: yup
        .number('Solo Numeros')
        .required('Inicial es requerido'),
    stok: yup
        .number('Solo Numeros')
        .required('Stok es requerido'),
    inventario: yup
        .number('Solo Numeros')
        .required('Inventario es requerido'),
    image: yup
        .string('Ingrese la descripcion')
        .required('descripcion es obligatoria'),
})

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, margin: "20px auto",
        maxWidth: 800
    },
    btnStyle: {
        margin: '8px 0'
    },
    input: {
        display: 'none',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    rootForm: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}))

export const ModalProducto = ({ categorias }) => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const { userInfo } = useSelector(state => state.userLogin);
    const { productoModalOpen, productoModal } = useSelector(state => state.modalProductoForm)
    const handleClose = () => dispatch(closeModalProducto());
    const [uploading, setUploading] = useState(false)

    const uploadFileHanlder = async (e, props) => {
        const file = e.target.files[0]
        const formData = new FormData()
        formData.append('image', file)
        setUploading(true)
        try {
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `${userInfo.token}`
                }
            }
            const { data } = await axios.post(`${baseUrl}/upload/productos/${productoModal.id_cobro}`, formData, config)
            props.setFieldValue('image', data)
            setUploading(false)
        } catch (error) {
            console.log(error)
            setUploading(false)
        }
    }

    return (
        <Modal
            closeAfterTransition
            open={productoModalOpen}
            BackdropProps={{
                timeout: 500
            }}
            onClose={handleClose}
            className={classes.modal}
        >
            <Paper elevation={5} className={classes.paperStyle} >
                <Formik onSubmit={(props) => {

                    dispatch(updateproducto(props, productoModal.id_cobro))
                    handleClose()
                    history.push('/')
                }}
                    validationSchema={validationSchema}
                    className={classes.rootForm}
                    initialValues={{
                        codigo: productoModal.codigo ? productoModal.codigo : '',
                        nombre: productoModal.nombre ? productoModal.nombre : '',
                        descripcion: productoModal.descripcion ? productoModal.descripcion : '',
                        valor: productoModal.valor ? productoModal.valor : 0,
                        total: productoModal.total ? productoModal.total : 0,
                        impuesto: productoModal.impuesto ? productoModal.impuesto : 0,
                        precio1: productoModal.precio1 ? productoModal.precio1 : 0,
                        inicial: productoModal.inicial ? productoModal.inicial : 0,
                        stok: productoModal.stok ? productoModal.stok : 0,
                        inventario: productoModal.inventario ? productoModal.inventario : 0,
                        id_categoria: productoModal.id_categoria ? productoModal.id_categoria : 0,
                        image: productoModal.image ? productoModal.image : 0
                    }}
                >
                    {props => (
                        <form onSubmit={props.handleSubmit}>
                            <TextField
                                type="number"
                                label='Codigo'
                                placeholder='Ingrese el codigo'
                                name="codigo"
                                value={props.values.codigo}
                                onChange={props.handleChange}
                                error={props.touched.codigo && Boolean(props.errors.codigo)}
                                helperText={props.touched.codigo && props.errors.codigo}
                            />
                            <TextField
                                label='Nombre'
                                placeholder='Ingrese el nombre'
                                name="nombre"
                                value={props.values.nombre}
                                onChange={props.handleChange}
                                error={props.touched.nombre && Boolean(props.errors.nombre)}
                                helperText={props.touched.nombre && props.errors.nombre}
                            />
                            <TextField
                                label='Descripcion'
                                placeholder='Ingrese una descripcion'
                                name="descripcion"
                                value={props.values.descripcion}
                                onChange={props.handleChange}
                                error={props.touched.descripcion && Boolean(props.errors.descripcion)}
                                helperText={props.touched.descripcion && props.errors.descripcion}
                            />
                            <TextField
                                type="number"
                                label='Valor'
                                placeholder='Ingrese el valor'
                                name="valor"
                                value={props.values.valor}
                                onChange={props.handleChange}
                                error={props.touched.valor && Boolean(props.errors.valor)}
                                helperText={props.touched.valor && props.errors.valor}
                            />
                            <TextField
                                type="number"
                                label='Total'
                                placeholder='Ingrese el total'
                                name="total"
                                value={props.values.total}
                                onChange={props.handleChange}
                                error={props.touched.total && Boolean(props.errors.total)}
                                helperText={props.touched.total && props.errors.total}
                            />
                            <TextField
                                type="number"
                                label='Impuesto'
                                placeholder='Ingrese el impuesto'
                                name="impuesto"
                                value={props.values.impuesto}
                                onChange={props.handleChange}
                                error={props.touched.impuesto && Boolean(props.errors.impuesto)}
                                helperText={props.touched.impuesto && props.errors.impuesto}
                            />
                            <TextField
                                type="number"
                                label='Precio1'
                                placeholder='Ingrese el precio1'
                                name="precio1"
                                value={props.values.precio1}
                                onChange={props.handleChange}
                                error={props.touched.precio1 && Boolean(props.errors.precio1)}
                                helperText={props.touched.precio1 && props.errors.precio1}
                            />
                            <TextField
                                type="number"
                                label='Inicial'
                                placeholder='Ingrese el inicial'
                                name="inicial"
                                value={props.values.inicial}
                                onChange={props.handleChange}
                                error={props.touched.inicial && Boolean(props.errors.inicial)}
                                helperText={props.touched.inicial && props.errors.inicial}
                            />
                            <TextField
                                type="number"
                                label='Stock'
                                placeholder='Ingrese el stok'
                                name="stok"
                                value={props.values.stok}
                                onChange={props.handleChange}
                                error={props.touched.stok && Boolean(props.errors.stok)}
                                helperText={props.touched.stok && props.errors.precio1}
                            />
                            <TextField
                                type="number"
                                label='Inventario'
                                placeholder='Ingrese el inventario'
                                name="inventario"
                                value={props.values.inventario}
                                onChange={props.handleChange}
                                error={props.touched.inventario && Boolean(props.errors.inventario)}
                                helperText={props.touched.inventario && props.errors.inventario}
                            />
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="id_categoria">Categoria</InputLabel>
                                <Select

                                    name="id_categoria"
                                    value={props.values.id_categoria || "DEFAULT"}
                                    onChange={props.handleChange}
                                    defaultValue="Default"

                                >
                                    <MenuItem value="Default" disabled>
                                        Eliga un puesto
                                    </MenuItem>

                                    {
                                        categorias.map((categoria, index) => (
                                            <MenuItem value={categoria.id_grupo} key={index}>{categoria.nombre}</MenuItem>
                                        ))
                                    }

                                </Select>
                            </FormControl>
                            <input accept="image/*" className={classes.input} id="icon-button-file" type="file" onChange={(e) => uploadFileHanlder(e, props)} />
                            <label htmlFor="icon-button-file">
                                <IconButton color="primary" aria-label="upload picture" component="span"  >
                                    <PhotoCamera />
                                </IconButton>
                                {uploading && <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />}
                            </label>
                            <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} >Enviar</Button>
                        </form>
                    )}

                </Formik>
            </Paper>
        </Modal>
    )
}

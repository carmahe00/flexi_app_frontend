import React from 'react'
import { Card, Grid, CardHeader, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';

import { openModalProducto } from '../actions/modalActions'
import withoutCategory from '../assets/category.svg'

const baseUrl = process.env.REACT_APP_API_URL_BASE

const useStyles = makeStyles((theme) => ({
    
    image: {
        height: 'auto',
        width: 'auto',
        maxHeight: '100px',
        maxWidth: '100px',
        margin: '10px',
        borderRadius: '3px',
        backgroundSize: 'cover',
        float: 'left'
    },
    card: {
        maxWidth: 1000,
    }
}));

export const CardProducto = ({ producto }) => {
    
    const classes = useStyles();
    const dispatch = useDispatch()
    const openModal = () => {
        dispatch(openModalProducto(producto))
    }
    return (
        <Grid item xs={12} sm={3} >
            <Card className={classes.card} onClick={openModal} >
                <CardHeader title={producto.nombre} />

                <Grid item container direction="row" justifyContent="center" >


                    <Grid item container direction="column" md alignContent="center" >
                        {
                            producto.image
                            ? <img className={classes.image} alt="Imagen" src={`${baseUrl}${producto.image}`} />
                            : <img className={classes.image} alt="Imagen" src={withoutCategory} />
                        }
                        <Typography color="primary" align="center" >
                            ${producto.precio1}
                        </Typography>
                    </Grid>
                    <Grid item container direction="column" md alignContent="center" >
                        <Typography color="textSecondary">
                            {producto.descripcion}
                        </Typography>
                    </Grid>
                </Grid>
            </Card>
        </Grid>
    )
}

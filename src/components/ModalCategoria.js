import React, { useEffect, useRef, useState } from 'react'
import * as yup from 'yup';
import { Button, CircularProgress, IconButton, Modal, Paper, TextField } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { PhotoCamera } from '@material-ui/icons';
import { useFormik } from 'formik';
import axios from 'axios';

import { closeModalCategoria } from '../actions/modalActions'
import { updateCategoria } from '../actions/categoriaActions';
const baseUrl = process.env.REACT_APP_API_URL

const validationSchema = yup.object({
    codigo: yup.number('Ingrese el codigo')
        .required('El codigo es obligatorio'),
    nombre: yup
        .string('Ingrese el nombre')
        .required('Nombre es obligatorio'),
    descripcion: yup
        .string('Ingrese la descipcion')
        .required('Descipcion es obligatoria'),
    image: yup
        .string('Ingrese la descipcion')
        .required('Descipcion es obligatoria'),
})

const useStyles = makeStyles(theme => ({
    paperStyle: {
        padding: 20, margin: "20px auto"
    },
    btnStyle: {
        margin: '8px 0'
    },
    input: {
        display: 'none',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}))

export const ModalCategoria = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const { userInfo } = useSelector(state => state.userLogin)
    const { categoriaModalOpen, categoriaModal } = useSelector(state => state.modalForm)
    const handleClose = () => dispatch(closeModalCategoria());
    const [uploading, setUploading] = useState(false)


    const formik = useFormik({
        initialValues: {
            codigo: '',
            nombre: '',
            descripcion: '',
            image: ''
        },
        validationSchema: validationSchema,
        onSubmit: (props) => {

            console.log(props)
            dispatch(updateCategoria(props, categoriaModal.id_grupo))
            handleClose()
        },
    });
    const formikRef = useRef();
    formikRef.current = formik

    const uploadFileHanlder = async (e) => {
        const file = e.target.files[0]
        const formData = new FormData()
        formData.append('image', file)
        setUploading(true)
        try {
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `${userInfo.token}`
                }
            }
            const { data } = await axios.post(`${baseUrl}/upload/${categoriaModal.id_grupo}`, formData, config)
            console.log(data)
            formik.setFieldValue('image', data)
            setUploading(false)
        } catch (error) {
            console.log(error)
            setUploading(false)
        }
    }


    useEffect(() => {
        if (formikRef.current)
            if (categoriaModalOpen && categoriaModal) {
                formikRef.current.setFieldValue('nombre', categoriaModal.nombre)
                formikRef.current.setFieldValue('descripcion', categoriaModal.descripcion)
                categoriaModal.codigo && formikRef.current.setFieldValue('codigo', categoriaModal.codigo)
                categoriaModal.image && formikRef.current.setFieldValue('image', categoriaModal.image)
            }

    }, [categoriaModalOpen, categoriaModal]);

    return (
        <Modal
            open={categoriaModalOpen}
            closeAfterTransition
            BackdropProps={{
                timeout: 500
            }}
            onClose={handleClose}
            className={classes.modal}
        >
            <Paper elevation={5} className={classes.paperStyle} >
                <form onSubmit={formik.handleSubmit} >
                    <TextField fullWidth
                        type="number"
                        label='Codigo'
                        placeholder='Ingrese el codigo'
                        name="codigo"
                        value={formik.values.codigo}
                        onChange={formik.handleChange}
                        error={formik.touched.codigo && Boolean(formik.errors.codigo)}
                        helperText={formik.touched.codigo && formik.errors.codigo}
                    />
                    <TextField fullWidth
                        label='Nombre'
                        placeholder='Ingrese el nombre'
                        name="nombre"
                        value={formik.values.nombre}
                        onChange={formik.handleChange}
                        error={formik.touched.nombre && Boolean(formik.errors.nombre)}
                        helperText={formik.touched.nombre && formik.errors.nombre}
                    />
                    <TextField fullWidth
                        label='Descripcion'
                        placeholder='Ingrese una descripcion'
                        name="descripcion"
                        value={formik.values.descripcion}
                        onChange={formik.handleChange}
                        error={formik.touched.descipcion && Boolean(formik.errors.descripcion)}
                        helperText={formik.touched.descripcion && formik.errors.descripcion}
                    />
                    <input accept="image/*" className={classes.input} id="icon-button-file" type="file" onChange={uploadFileHanlder} />
                    <label htmlFor="icon-button-file">
                        <IconButton color="primary" aria-label="upload picture" component="span"  >
                            <PhotoCamera />
                        </IconButton>
                        {uploading && <CircularProgress style={{ width: '100px', height: '100px', margin: 'auto', display: 'block', marginTop: '50px' }} color="primary" />}
                    </label>
                    <Button type='submit' color='primary' variant="contained" className={classes.btnStyle} fullWidth>Enviar</Button>
                </form>
            </Paper>
        </Modal>
    )
}

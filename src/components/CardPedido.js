import React, { useEffect, useRef, useState } from 'react'
import { Button, Card, CardContent, CardHeader, Grid, TableCell, Table, TableRow, TableBody } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Send } from '@material-ui/icons';
import moment from 'moment'
import { useDispatch } from 'react-redux';

import { changeDetailPedido } from '../actions/pedidoPActions';

const useStyles = makeStyles((theme) => ({
    cardHeaderRoot: {
        backgroundColor: theme.palette.primary.main
    },

    cardHeaderSubTitleRoot: {
        ...theme.typography.tab,
        textAlign: 'center',
        color: 'white'
    },
    footer: {
        backgroundColor: theme.palette.primary.main,
        ...theme.typography.tab,
        textAlign: 'center',
        color: 'white'
    }
}));

export const CardPedido = ({ pedido }) => {

    const classes = useStyles()
    const dispatch = useDispatch()

    //Timer
    const [timer, setTimer] = useState(0)
    const increment = useRef(null)


    const changeState = (pedidoId, despachoId) => {
        dispatch(changeDetailPedido(pedidoId, despachoId))
    }

    const handleStart = ({ fecha_inicio }) => {
        const now = moment()
        const startTime = moment(fecha_inicio)
        //express as a duration
        const diffDuration = moment.duration(now.diff(startTime));
        setTimer(diffDuration.asSeconds())

        increment.current = setInterval(() => {
            setTimer((timer) => timer + 1)
        }, 1000)
    }

    const handleReset = () => {
        clearInterval(increment.current)
        setTimer(0)
    }


    useEffect(() => {
        handleStart(pedido.pedidoP)
        return () => handleReset()
    }, [pedido])

    const formatTime = ({ fecha_inicio }) => {
        return moment(fecha_inicio).startOf('days')
            .seconds(timer)
            .format('mm:ss');

    }

    return (
        <Grid item xs={12} sm={3} >
            <Card elevation={6} >
                <CardHeader
                    title={pedido.pedidoP.puesto}
                    subheader={`(${pedido.pedidoP.id_pedido})`}
                    classes={{ root: classes.cardHeaderRoot }}
                    titleTypographyProps={{
                        classes: {
                            root: classes.cardHeaderSubTitleRoot
                        }
                    }}
                    subheaderTypographyProps={{
                        classes: {
                            root: classes.cardHeaderSubTitleRoot
                        }
                    }}
                />
                <CardContent>
                    <Table aria-label="simple table" size="small" >
                        <TableBody>
                            {
                                pedido.pedidosDetalle.map((pedidoDetalles, index) => (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row"  >
                                            {`${Number(pedidoDetalles.cantidad)} ${pedidoDetalles.nombre} (${pedidoDetalles.descripcion})`}
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>

                </CardContent>
                {
                    <Grid container justifyContent="flex-end" className={classes.footer}>
                        <Button variant="contained" color="secondary" endIcon={<Send />} onClick={() => changeState(pedido.pedidoP.id_pedido, pedido.pedidoP.id_despacho)} >
                            {formatTime(pedido.pedidoP)}
                        </Button>
                    </Grid>
                }
            </Card>
        </Grid >
    )


}
